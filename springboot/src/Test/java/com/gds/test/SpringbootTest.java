package com.gds.test;

import com.gds.Application;
import com.gds.service.IUserService;
import com.gds.utils.redis.RedisUtils;
import org.junit.Test;
import org.junit.runner.RunWith;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.SortedMap;
import java.util.TreeMap;
//import org.springframework.test.context.web.WebAppConfiguration;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@WebAppConfiguration
public class SpringbootTest {

    @Autowired
    private IUserService userService;
    @LocalServerPort
    private int port;
    @Test
    public  void  test(){
        System.out.println("测试"+ port);
    }

    @Resource
    private RedisUtils redisUtils;
    @Test
    public  void  testRedis(){
        redisUtils.set("test","China");
        redisUtils.set("cn","cn");
        redisUtils.set("bj","beijing");
        redisUtils.set("hk","HongKong");
        redisUtils.set("MAC","China");
        redisUtils.set("TW","ChineseTaipei");
        System.out.println(redisUtils.get("test"));
    }
    @Test
    public void testMap(){
        SortedMap map  =  new TreeMap<>();
//        map.put(null,"");
        System.out.println(map.toString());
    }
}
