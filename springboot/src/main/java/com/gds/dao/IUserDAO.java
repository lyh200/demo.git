/*
 * Copyright (c) 2013-2014, thinkjoy Inc. All Rights Reserved.
 *
 * Project Name: gds
 * $Id:  UserDAO.java 2016-11-19 21:19:34 $
 */
package com.gds.dao;

import com.gds.dao.common.IBaseDAO;
import com.gds.domain.MerchantStatisticsItem;
import com.gds.domain.User;
import com.gds.domain.dto.StudentDto;
import com.gds.domain.dto.TeacherDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
@Mapper
public interface IUserDAO extends IBaseDAO<User> {

    public List<StudentDto> getStudentsForPage(@Param("condition") Map<String, Object> condition, @Param("offset") int offset, @Param("rows") int rows,
                                               @Param("orderBy") String orderBy, @Param("sortBy") String sortBy);

    public  Integer   getStudentsCount(@Param("condition") Map<String, Object> condition);
    public StudentDto getStudentById(@Param("id") String id);


    public List<TeacherDto> getTeachersForPage(@Param("condition") Map<String, Object> condition, @Param("offset") int offset, @Param("rows") int rows,
                                               @Param("orderBy") String orderBy, @Param("sortBy") String sortBy);

    public  Integer   getTeachersCount(@Param("condition") Map<String, Object> condition);
    public TeacherDto getTeacherById(@Param("id") String id);

    public void insertBatch(List<MerchantStatisticsItem> list);
}
