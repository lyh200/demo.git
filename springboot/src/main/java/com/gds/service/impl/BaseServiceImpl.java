package com.gds.service.impl;

import com.gds.common.BaseDomain;
import com.gds.dao.common.IBaseDAO;
import com.gds.utils.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;


/**
 * Created by liuyonghui on 16/11/19.
 * 此类提供service 共有方法实现模板
 */
public class BaseServiceImpl<T extends BaseDomain, X extends IBaseDAO> {

    @Autowired
    X baseDao;

    public void insert(T entity) {
        baseDao.insert(entity);
    }

    public int update(T entity) {
        return baseDao.update(entity);
    }

    public int updateMap(Map<String, Object> entityMap) {
        return baseDao.updateMap(entityMap);
    }

    public T findOne(String property, Object value) {
        return (T) baseDao.findOne(property, value, "status", StatusEnum.Normal.getIndex(), null, null);
    }

    public List<T> queryList(Map map, String orderBy, String sortBy){
        return (List<T>) baseDao.queryList(map,orderBy,sortBy,null);
    }
    public List<T> queryPage(Map map , int pageNo , int pageSize , String order,String sort){
        return (List<T>) baseDao.queryPage(map,pageNo,pageSize,order,sort, null);
    }

    public int count(Map<String, Object> condition){
        return baseDao.count(condition);
    }

}
