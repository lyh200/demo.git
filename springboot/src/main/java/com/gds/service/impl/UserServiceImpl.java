
package com.gds.service.impl;

import com.gds.dao.IUserDAO;
import com.gds.domain.MerchantStatisticsItem;
import com.gds.domain.User;
import com.gds.domain.dto.StudentDto;
import com.gds.domain.dto.TeacherDto;
import com.gds.service.IUserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("UserServiceImpl")
public class UserServiceImpl extends BaseServiceImpl<User, IUserDAO> implements IUserService {


    public List<StudentDto> getStudentsForPage(Map<String, Object> condition, int offset, int rows, String orderBy, String sortBy) {
        return baseDao.getStudentsForPage(condition,offset,rows,orderBy,sortBy);
    }

    public Integer getStudentsCount(Map<String, Object> condition) {
        return baseDao.getStudentsCount(condition);
    }

    public StudentDto getStudentById(String id) {
        return baseDao.getStudentById(id);
    }

    public List<TeacherDto> getTeachersForPage(Map<String, Object> condition, int offset, int rows, String orderBy, String sortBy) {
        return baseDao.getTeachersForPage(condition,offset,rows,orderBy,sortBy);
    }

    public Integer getTeachersCount(Map<String, Object> condition) {
        return baseDao.getTeachersCount(condition);
    }

    public TeacherDto getTeacherById(String id) {
        return baseDao.getTeacherById(id);
    }

    public void insertBatch(List<MerchantStatisticsItem> list) {
        baseDao.insertBatch(list);
    }
}
