package com.gds.service.impl;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class WxProperties {
    private String appId;
    private String mchId;
    private String notifyUrl;
    private String api_key;
    private String Appsecret;
    private String token;
    private String BaseUrl;
    private String redirect;
}
