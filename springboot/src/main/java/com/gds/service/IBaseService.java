package com.gds.service;

import com.gds.common.BaseDomain;

import java.util.List;
import java.util.Map;

/**
 * Created by liuyonghui on 16/11/19.
 * service层  个各类 共同的方法 提取
 */
public interface IBaseService<T extends BaseDomain> {


    public void insert(T entity);

    public int update(T entity);

    public int updateMap(Map<String, Object> entityMap);

//    public T findOne(String property, Object value, String orderBy, String sortBy);
//
//    public T findOne(String property, Object value, String property1, Object value1, String orderBy, String sortBy);

    public T findOne(String property, Object value);

    /***
     * 根据 条件获取数据
     * @param map
     * @param orderBy
     * @param sortBy
     * @return
     */
    public List<T> queryList(Map map, String orderBy, String sortBy);


    public List<T> queryPage(Map map, int pageNo, int pageSize, String order, String sort);

    public int count(Map<String, Object> condition);

}
