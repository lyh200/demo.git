

package com.gds.service;


import com.gds.domain.MerchantStatisticsItem;
import com.gds.domain.User;
import com.gds.domain.dto.StudentDto;
import com.gds.domain.dto.TeacherDto;

import java.util.List;
import java.util.Map;

public interface IUserService extends IBaseService<User> {

    public List<StudentDto> getStudentsForPage(Map<String, Object> condition, int offset, int rows,
                                               String orderBy, String sortBy);

    public  Integer   getStudentsCount(Map<String, Object> condition);

    public StudentDto getStudentById(String id);

    public List<TeacherDto> getTeachersForPage(Map<String, Object> condition, int offset, int rows,
                                               String orderBy, String sortBy);

    public  Integer   getTeachersCount(Map<String, Object> condition);

    public TeacherDto getTeacherById(String id);

    void insertBatch(List<MerchantStatisticsItem> list);

}
