package com.gds.service;

import org.springframework.scheduling.annotation.Async;

import javax.servlet.http.HttpServletRequest;

public interface AysnService {
    @Async
    void getSy(HttpServletRequest request);
}
