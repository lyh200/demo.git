package com.gds.service;

import com.gds.common.wechat.wechat.entities.Oauth2AccessTokenResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface WechatService {

    /***
     *  H5、公众号支付订单 demo
     */
    public Map<String, Object> unifiedorderJSAPI(String orderCode,
                                                 int money,
                                                 String clientIp,
                                                 String openId) throws Exception;

    public Oauth2AccessTokenResult getAccessToken(String code);


    /***
     * 返回 js引用配置
     * @param url
     * @return
     */
    public Map<String, Object>  getJsConfig(String url);

    /***
     * 支付回调 demo
     * @param request
     * @param response
     * @throws Exception
     */
    void wxPayStatus(HttpServletRequest request, HttpServletResponse response) throws Exception;
}
