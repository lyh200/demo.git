package com.gds.controller;


import com.gds.aspect.LogInfo;
import com.gds.domain.User;
import com.gds.service.AysnService;
import com.gds.service.IUserService;
import com.gds.utils.BizException;
import com.gds.utils.Config;
import com.gds.utils.redis.RedisUtils;
import com.gds.config.webscoket.WebSocketDemo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;


@Controller
@Api(value = "测试Swagger2",description="简单的API")
public class SampleController {


    @Autowired
    private IUserService userService;

    @Value("${test.name}")
    private String name;
    @Value("${test.sex}")
    private String sex;
    @Autowired
    private Config config;
    @Resource
    private RedisUtils redisUtils;

    @Resource
    private AysnService aysnService;



    @ApiOperation(value = "用户登录", notes = "用户登录")
    @LogInfo(operationDesc ="用户登录")
    @RequestMapping(value="/login",method=RequestMethod.GET)
    @ResponseBody
    public User userLogin(HttpServletRequest request,
                          HttpServletResponse response,
                          @ApiParam(name = "account", value = "用户账号", required = true) @RequestParam("account") String account) {
        if (account == null || ("").equals(account.trim())) {
            throw new BizException("101", "账号不能为空", "");
        }
        //查证账号密码是否一致
        User user = userService.findOne("code", account);
        if (user == null) {
            throw new BizException("999", "用户不存在");
        }
        return user;

    }

    /***
     * 测试向某个用户发送消息 通过 websocket
     * @param userId
     * @param message
     * @throws IOException
     */
    @ApiOperation(value = "websocket测试", notes = "websocket测试")
    @RequestMapping(value ="/test" ,method=RequestMethod.GET)
    @ResponseBody
    public void userLogin(String userId,String message) throws IOException {
        WebSocketDemo.sendMessageTo(userId,message);
    }

    @RequestMapping(value = "/testRedis" ,method=RequestMethod.GET)
    @ResponseBody
    public String  getValueByKey(String key){
        return (String) redisUtils.get(key);
    }

    @RequestMapping(value="/test2",method=RequestMethod.GET)
    @ResponseBody
    public List test2(){
        List  list  = new ArrayList();
        SortedMap  map  =  new TreeMap<>();
        map.put("CN","CHINA");
        map.put("USA","美国");
        map.put("UK","英国");
        map.put("RUS","俄罗斯");
        map.put("JP","日本");
        map.put("SKOR","俄罗斯");
        map.put("AUS","澳大利亚");
        map.put("dsa","test");
        list.add(map);
        return list;
    }

    @ApiOperation(value = "redisson测试", notes = "websocket测试")
    @RequestMapping(value="/redisson",method=RequestMethod.GET)
    @ResponseBody
    public boolean test4(HttpServletRequest request){
        aysnService.getSy(request);
        System.out.println("主线");
        return  true;
    }




}