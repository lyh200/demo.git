package com.gds.utils;

/**
 * Created by liuyonghui on 16/11/21.
 */
public enum StatusEnum {
    Normal(10, "正常状态"),
    delete(30, "逻辑删除");


    private String name;
    private int index;

    private StatusEnum(int index, String name) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
