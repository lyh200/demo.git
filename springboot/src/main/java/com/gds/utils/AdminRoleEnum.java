package com.gds.utils;

/**
 * Created by liuyonghui on 2/14/17.
 */
public enum AdminRoleEnum {
    superAdmin(0, "超级管理员"),
    majorAdmin(1, "院系管理员");

    private String name;
    private Integer index;

    private AdminRoleEnum(Integer index, String name) {
        this.index = index;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
