package com.gds.utils;

/**
 * Created by liuyonghui on 16/11/21.
 */
public enum SexEnum {
    boy(1, "男"),
    girl(2, "女"),
    unkown(0, "未知");


    private String name;
    private int index;

    private SexEnum(int index, String name) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }


}
