package com.gds.utils;

/**
 * Created by liuyonghui on 16/11/26.
 *
 * 学生信息是否确认
 */
public enum ConfirmEnum {
    isConfirm(1, "确认"),
    noConfirm(0, "未确认");


    private int index;
    private String name;


    private ConfirmEnum(int index, String name) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
