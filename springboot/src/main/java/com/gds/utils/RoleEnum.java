package com.gds.utils;

/**
 * Created by liuyonghui on 16/11/21.
 */
public enum RoleEnum {
    student("META-INF.resources", "学生"),
    teacher("2", "老师"),
    admin("3", "管理员");


    private String name;
    private String index;

    private RoleEnum(String index, String name) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

}
