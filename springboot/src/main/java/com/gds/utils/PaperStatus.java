package com.gds.utils;

/**
 * Created by liuyonghui on 16/12/7.
 */
public enum PaperStatus {

    unselected("0", "未选状态"),
    selected("META-INF.resources", "已选状态"),
     teacherGuide("2", "2导师指导"),
    upload("3", "教师指导"),
    Pending("4", "审核"),
    reply("5", "通过"),
    pass("6", "不通过");
    private String name;
    private String index;

    private PaperStatus(String index, String name) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

}
