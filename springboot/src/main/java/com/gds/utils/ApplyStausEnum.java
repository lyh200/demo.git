package com.gds.utils;

/**
 * Created by liuyonghui on 2/8/17.
 */
public enum  ApplyStausEnum {
    studentCommit(10, "学生已申请论题"),
    teacherCommit(20, "导师通过,等待系统确认"),
    teacherNoCommit(30, "导师不通过"),
    SystemCommit(40, "系统通过"),
    SystemNoCommit(50, "系统不通过");


    private String name;
    private int index;

    private ApplyStausEnum(int index, String name) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
