package com.gds.utils;

/**
 * Created by liuyonghui on 16/11/21.
 */
public enum ProfessionalTitleEmum {

    TeachingAssistant(1, "助教",3),
    lecturer(2, "讲师",4),
    associateProfessor(3, "副教授",5),
    professor(4, "教授",6);


    private String name;
    private int index;
    private  int limit;



    private ProfessionalTitleEmum(int index, String name, int limit) {
        this.name = name;
        this.index = index;
        this.limit = limit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
