package com.gds.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
<<<<<<< Updated upstream
 * Created by crj on 2017/11/6.
=======
 * Created by eason on 2017/11/5.
>>>>>>> Stashed changes
 */
@Component
@ConfigurationProperties(prefix = "test")
public class Config {

    private String name;
    private String sex;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
