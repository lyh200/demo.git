package com.gds.config.exception;

import org.apache.log4j.Logger;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by eason on 2017/9/23.
 */
@Component
@ControllerAdvice
public class MyResponseBodyAdvice implements ResponseBodyAdvice<Object> {
    private static Logger log = Logger.getLogger(MyResponseBodyAdvice.class);
    private final static String[] notDealUrl = {"swagger","/v2/api-docs","/error"};
    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
//        log.debug("MyResponseBodyAdvice==>supports:" + converterType);
//        log.debug("MyResponseBodyAdvice==>supports:" + returnType.getClass());
//        log.debug("MyResponseBodyAdvice==>supports:"
//                + MappingJackson2HttpMessageConverter.class.isAssignableFrom(converterType));
//        return MappingJackson2HttpMessageConverter.class.isAssignableFrom(converterType);
        return  true;

    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
     String url =request.getURI().getPath();
     for(String key:notDealUrl) {
         if (url.contains(key)){
             return body;
         }
     }
        Map map =new HashMap();
        map.put("code","200");
        map.put("msg","成功");
        map.put("data",body);
        map.put("success",true);
        return map;
    }
}
