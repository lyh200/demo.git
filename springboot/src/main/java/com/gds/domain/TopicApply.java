


package com.gds.domain;


import com.gds.common.CreateBaseDomain;

public class TopicApply extends CreateBaseDomain<Integer> {
    /**  */
    private Integer uid;
    /** 申请的论文题目 */
    private String name;
    /** 申请的论文题目 */
    private String description;
    /** 10 提交 20通过  30 不通过 */
    private Integer applyStatus;

	public TopicApply(){
	}
    public void setUid(Integer value) {
        this.uid = value;
    }

    public Integer getUid() {
        return this.uid;
    }
    public void setName(String value) {
        this.name = value;
    }

    public String getName() {
        return this.name;
    }
    public void setDescription(String value) {
        this.description = value;
    }

    public String getDescription() {
        return this.description;
    }
    public void setApplyStatus(Integer value) {
        this.applyStatus = value;
    }

    public Integer getApplyStatus() {
        return this.applyStatus;
    }


}

