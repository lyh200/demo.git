package com.gds.domain;



import com.gds.common.CreateBaseDomain;


public class User extends CreateBaseDomain<Long> {
    /** 学生为学号，教师为教工号 */
    private String code;
    /** 密码 */
    private String password;
    /** 姓名 */
    private String name;
    /** 性别0未知，1男2女 */
    private Integer sex;
    /** 手机 */
    private String phone;
    /** 电子邮件 */
    private String email;
    /** headImage */
    private String headImage;
    /**  */
    private String idcard;
    /**  用户身份 1学生 2教师 3管理员 与gds_role无关*/
    private String roleId;
    /** 1 教授 2副教授 3讲师 */
    private Integer professionalTitleId;
    /** 学生信息是否确认 0 未确认  1已经确认 */
    private Integer isConfirm;

	public User(){
	}
    public void setCode(String value) {
        this.code = value;
    }

    public String getCode() {
        return this.code;
    }
    public void setPassword(String value) {
        this.password = value;
    }

    public String getPassword() {
        return this.password;
    }
    public void setName(String value) {
        this.name = value;
    }

    public String getName() {
        return this.name;
    }
    public void setSex(Integer value) {
        this.sex = value;
    }

    public Integer getSex() {
        return this.sex;
    }
    public void setPhone(String value) {
        this.phone = value;
    }

    public String getPhone() {
        return this.phone;
    }
    public void setEmail(String value) {
        this.email = value;
    }

    public String getEmail() {
        return this.email;
    }
    public void setHeadImage(String value) {
        this.headImage = value;
    }

    public String getHeadImage() {
        return this.headImage;
    }
    public void setIdcard(String value) {
        this.idcard = value;
    }

    public String getIdcard() {
        return this.idcard;
    }
    public void setRoleId(String value) {
        this.roleId = value;
    }

    public String getRoleId() {
        return this.roleId;
    }
    public void setProfessionalTitleId(Integer value) {
        this.professionalTitleId = value;
    }

    public Integer getProfessionalTitleId() {
        return this.professionalTitleId;
    }
    public void setIsConfirm(Integer value) {
        this.isConfirm = value;
    }

    public Integer getIsConfirm() {
        return this.isConfirm;
    }


}

