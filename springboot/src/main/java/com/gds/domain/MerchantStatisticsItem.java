package com.gds.domain;


import java.math.BigDecimal;
import java.util.Date;


public class MerchantStatisticsItem {

    private Long id;

    /**
     * 商户码
     */

    private Integer merchantId;

    /**
     * 时间
     */
    private Date time;

    /**
     * 项目id
     */

    private Long itemId;

    /**
     * 服务项目名称
     */

    private String itemName;

    /**
     * 数量
     */
    private Integer total;

    /**
     * 收入
     */
    private BigDecimal amount;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取商户码
     *
     * @return merchant_id - 商户码
     */
    public Integer getMerchantId() {
        return merchantId;
    }

    /**
     * 设置商户码
     *
     * @param merchantId 商户码
     */
    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 获取时间
     *
     * @return time - 时间
     */
    public Date getTime() {
        return time;
    }

    /**
     * 设置时间
     *
     * @param time 时间
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * 获取项目id
     *
     * @return item_id - 项目id
     */
    public Long getItemId() {
        return itemId;
    }

    /**
     * 设置项目id
     *
     * @param itemId 项目id
     */
    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    /**
     * 获取服务项目名称
     *
     * @return item_name - 服务项目名称
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * 设置服务项目名称
     *
     * @param itemName 服务项目名称
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /**
     * 获取数量
     *
     * @return total - 数量
     */
    public Integer getTotal() {
        return total;
    }

    /**
     * 设置数量
     *
     * @param total 数量
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * 获取收入
     *
     * @return amount - 收入
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 设置收入
     *
     * @param amount 收入
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}