package com.gds.domain.dto;

import com.gds.domain.TopicApply;

/**
 * Created by liuyonghui on 2/8/17.
 */
public class TopicApplyDto extends TopicApply {

    private String  ApplyStatusCN;

    public String getApplyStatusCN() {
        return ApplyStatusCN;
    }

    public void setApplyStatusCN(String applyStatusCN) {
        ApplyStatusCN = applyStatusCN;
    }
}
