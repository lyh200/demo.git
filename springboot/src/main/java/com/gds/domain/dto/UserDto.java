package com.gds.domain.dto;

import com.gds.domain.User;

/**
 * Created by liuyonghui on 16/11/23.
 */
public class UserDto extends User {
    private String className;
    private String majorName;
    private String department;
    private String schoolYear;
    private Integer limit;
    private Integer spare;
    private String professionalTitle;
    private Integer chooseStatus;
    private Integer topicTotal;
    private String  lastTopicSatus;

    private  TopicApplyDto  deal;

    public String getProfessionalTitle() {
        return professionalTitle;
    }

    public void setProfessionalTitle(String professionalTitle) {
        this.professionalTitle = professionalTitle;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMajorName() {
        return majorName;
    }

    public void setMajorName(String majorName) {
        this.majorName = majorName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
    public Integer getLimit() {
        return limit;
    }
    public Integer getSpare() {
        return spare;
    }

    public void setSpare(Integer spare) {
        this.spare = spare;
    }

    public Integer getChooseStatus() {
        return chooseStatus;
    }

    public void setChooseStatus(Integer chooseStatus) {
        this.chooseStatus = chooseStatus;
    }

    public Integer getTopicTotal() {
        return topicTotal;
    }

    public void setTopicTotal(Integer topicTotal) {
        this.topicTotal = topicTotal;
    }

    public String getLastTopicSatus() {
        return lastTopicSatus;
    }

    public void setLastTopicSatus(String lastTopicSatus) {
        this.lastTopicSatus = lastTopicSatus;
    }
    public TopicApplyDto getDeal() {
        return deal;
    }

    public void setDeal(TopicApplyDto deal) {
        this.deal = deal;
    }
}
