package com.gds.domain.dto;



import com.gds.utils.validation.ValidationResult;
import com.gds.utils.validation.ValidationUtils;

import java.util.Map;

public class Test {

    public static void main(String[] args) {

        Person person = new Person();
        person.setAge(12);
        person.setGender(2);
//       person.setName("李智龙");
        ValidationResult result = ValidationUtils.validateEntity(person);
        Map<String, String> map = result.getErrorMsg();
        boolean isError = result.isHasErrors();
        System.out.println("isError: " +isError);
        System.out.println(map);
    }

}
