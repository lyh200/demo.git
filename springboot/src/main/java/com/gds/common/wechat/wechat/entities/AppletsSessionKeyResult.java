package com.gds.common.wechat.wechat.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/***
*
* { 
* 	"openid": "OPENID",
* 	"session_key": "SESSIONKEY"
* 
* }
*/

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class AppletsSessionKeyResult extends BaseResult {

	
	private String openid;
	private String session_key;
	
	
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getSession_key() {
		return session_key;
	}
	public void setSession_key(String session_key) {
		this.session_key = session_key;
	}
	
}
