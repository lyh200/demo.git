package com.gds.common.wechat.wechat.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/****
 *
 * { "openid":" OPENID",
 * " nickname": NICKNAME,
 * "sex":"1",
 * "province":"PROVINCE"
 * "city":"CITY",
 * "country":"COUNTRY",
 * "headimgurl":    "http://thirdwx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/46",
 * "privilege":[ "PRIVILEGE1" "PRIVILEGE2"     ],
 * "unionid": "o6_bmasdasdsad6_2sgVt7hMZOPfL"
 * }
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class UserInfoResult extends BaseResult {
    private String openid;
    private String nickname;
    private String province;
    private String city;
    private String country;
    private String headimgurl;
    private String privilege;
    private String unionid;
}
