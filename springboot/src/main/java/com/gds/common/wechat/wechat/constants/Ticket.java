package com.gds.common.wechat.wechat.constants;

import lombok.Data;

@Data
public class Ticket {

    public   String  value;

    /***
     * 过期时间毫秒数
     */
    private  Long   overdue;

    /***
     * 有效时间 秒
     */
    private  Long   expire;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getOverdue() {
		return overdue;
	}

	public void setOverdue(Long overdue) {
		this.overdue = overdue;
	}

	public Long getExpire() {
		return expire;
	}

	public void setExpire(Long expire) {
		this.expire = expire;
	}
}
