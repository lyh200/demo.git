package com.gds.common.wechat.wechat.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/***
 * 全局token获取
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CgiAccessTokenResult extends BaseResult {
    private String access_token;
    private Long expires_in;
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	public Long getExpires_in() {
		return expires_in;
	}
	public void setExpires_in(Long expires_in) {
		this.expires_in = expires_in;
	}
}
