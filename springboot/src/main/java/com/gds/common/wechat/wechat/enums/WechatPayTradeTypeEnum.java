package com.gds.common.wechat.wechat.enums;

public enum WechatPayTradeTypeEnum {
    APP支付("APP","APP支付"),
    公众号支付("JSAPI","公众号支付"),
    扫码支付("NATIVE","扫码支付");

    String code;
    String description;

    private WechatPayTradeTypeEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
