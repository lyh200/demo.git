package com.gds.common.wechat.wechat.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class AppletsInfoParams {

	
	private String signature;
	private String rawData;
	private String encryptedData;
	private String iv;
	private String sessionkey_3rd;
	private String merchantId;
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getRawData() {
		return rawData;
	}
	public void setRawData(String rawData) {
		this.rawData = rawData;
	}
	public String getEncryptedData() {
		return encryptedData;
	}
	public void setEncryptedData(String encryptedData) {
		this.encryptedData = encryptedData;
	}
	public String getIv() {
		return iv;
	}
	public void setIv(String iv) {
		this.iv = iv;
	}
	public String getSessionkey_3rd() {
		return sessionkey_3rd;
	}
	public void setSessionkey_3rd(String sessionkey_3rd) {
		this.sessionkey_3rd = sessionkey_3rd;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
}
