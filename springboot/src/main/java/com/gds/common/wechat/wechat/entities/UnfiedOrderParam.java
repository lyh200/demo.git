package com.gds.common.wechat.wechat.entities;

import lombok.Data;

@Data
public class UnfiedOrderParam {

    /***
     * 公众账号ID
     * TODO 必填
     */
    private  String  appid;
    /***
     * 商户号
     *TODO  必填
     */
    private  String  mch_id;
    /***
     * 设备号
     */
    private  String  device_info;

    /***
     * 随机字符串	nonce_str
     * TODO 必填
     */
    private String  nonce_str;

    /***
     *  签名	sign
     *  todo 必填
     */
    private  String  sign;

    /**
     * 签名类型	sign_type
     * todo 必填
     * MD5	签名类型，默认为MD5，支持HMAC-SHA256和MD5。
     */
    private String sign_type;

    /***
     * 商品描述
     * TODO 必填
     *
     */
    private String body;

    private String  out_trade_no;

    private String  fee_type;

    private int  total_fee;

    /***
     * APP和网页支付提交用户端ip，Native支付填调用微信支付API的机器IP。
     */
    private String spbill_create_ip;

    /***
     * JSAPI 公众号支付
     *
     * NATIVE 扫码支付
     *
     * APP APP支付
     */
    private String trade_type;

    private String openid;
    private String notify_url;
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getMch_id() {
		return mch_id;
	}
	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}
	public String getDevice_info() {
		return device_info;
	}
	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}
	public String getNonce_str() {
		return nonce_str;
	}
	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getSign_type() {
		return sign_type;
	}
	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getOut_trade_no() {
		return out_trade_no;
	}
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}
	public String getFee_type() {
		return fee_type;
	}
	public void setFee_type(String fee_type) {
		this.fee_type = fee_type;
	}
	public int getTotal_fee() {
		return total_fee;
	}
	public void setTotal_fee(int total_fee) {
		this.total_fee = total_fee;
	}
	public String getSpbill_create_ip() {
		return spbill_create_ip;
	}
	public void setSpbill_create_ip(String spbill_create_ip) {
		this.spbill_create_ip = spbill_create_ip;
	}
	public String getTrade_type() {
		return trade_type;
	}
	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getNotify_url() {
		return notify_url;
	}
	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}


}
