package com.gds.common.wechat.wechat.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/***
 * 微信支付回调函数 返回支付结果信息
 * 详细请参考微信官方文档
 *  https://pay.weixin.qq.com/wiki/doc/api/native.php?chapter=9_7&index=8
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PayNotifyResult {
    /***返回状态码	 SUCCESS/FAIL ***/
    private String return_code;
    private String return_msg;
    /***以下字段在return_code为SUCCESS的时候有返回****/
    //appid
    private String appid;
    //商户id
    private String mch_id;
    //设备号
    private String device_info;
    //随机字符串
    private String nonce_str;
    //签名
    private String sign;
    //签名类型
    private String sign_type;
    //业务结果 SUCCESS/FAIL
    private String result_code;
    private String err_code;
    private String err_code_des;
    private String openid;
    private String is_subscribe;
    private String trade_type;
    private String bank_type;
    private String total_fee;
    private String settlement_total_fee;
    private String fee_type;
    private String cash_fee;
    private String cash_fee_type;
    private String coupon_fee;
    private String coupon_count;
    private String transaction_id;
    private String out_trade_no;
    private String attach;
    private String time_end;

}
