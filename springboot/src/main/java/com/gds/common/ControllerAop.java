/*
package com.gds.common;

import com.alibaba.fastjson.JSONObject;
import com.gds.utils.BizException;
import com.google.gson.Gson;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

*/
/***
 * aop 处理请求日志记录、全局异常处理
 *//*

@Component
@Aspect
public class ControllerAop {

    private static Logger logger = LoggerFactory.getLogger(ControllerAop.class);


    @Pointcut("execution(* com.gds.controller.*.*(..))")
    public void init() {

    }

    @Before(value = "init()")
    public void before(JoinPoint jp) {
    }

    @AfterReturning(value = "init()")
    public void afterReturning() {
    }

    @AfterThrowing(value = "init()")
    public void throwss() {
    }

    @After(value = "init()")
    public void after() {
    }

    */
/**
     * 記錄日誌、全局異常處理
     *
     * @param pjp
     * @return
     * @throws Exception
     *//*

    @SuppressWarnings("rawtypes")
    @Around(value = "init()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        //是否記錄日誌
        boolean isLog = false;
        //是否記錄異常日誌
        boolean logException = false;
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("XDomainRequestAllowed", "META-INF.resources");
        String targetName = pjp.getTarget().getClass().getName();
        String methodName = pjp.getSignature().getName();
        logMethodParam(pjp, request);
        Object o = dealException(pjp, isLog, logException, response, targetName, methodName);
        return o;
    }

    */
/***
     * 异常处理
     * @param pjp  ProceedingJoinPoint
     * @param isLog 是否记录日志
     * @param logException  是否记录异常日志
     * @param response   response
     * @param targetName 类名
     * @param methodName 方法名
     * @return
     * @throws Throwable
     *//*

    private Object dealException(ProceedingJoinPoint pjp, boolean isLog, boolean logException, HttpServletResponse response, String targetName, String methodName) throws Throwable {
        logger.info("-------【方法 " + targetName + "." + methodName + "() 执行开始】.....");
        Object o = null;
        try {
            o = pjp.proceed();
        } catch (Throwable e) {
            //异常信息处理
            StackTraceElement[] ste = e.getStackTrace();
            List<StackTraceElement> stackList = new ArrayList<StackTraceElement>();
            for (StackTraceElement elemmet : ste) {
                if (StringUtils.contains(elemmet.getClassName(), "com.yingu")) {
                    stackList.add(elemmet);
                }
            }
            if (isLog) {
                if (logException) {
                    if (e instanceof BizException) {
                        logger.info("记录业务异常日志:" + e.toString() + ":" + e.getLocalizedMessage());
                    } else {
                        logger.info("记录异常日志:" + e.toString() + ":" + e.getLocalizedMessage());
                    }
                    if (e instanceof IllegalArgumentException) {

                    } else if (e instanceof Exception) {

                    } else {

                    }

                } else {

                }
            }
            exceptionHandler(response, e);
        }
        logger.info("-------【方法 " + targetName + "." + methodName + "() 执行结束】.....");
        return o;
    }

    */
/***
     * 打印请求路径参数
     * @param pjp pjp
     * @param request request
     *//*

    private void logMethodParam(ProceedingJoinPoint pjp, HttpServletRequest request) {
        String uri = request.getRequestURI();
        logger.info("访问uri：{}", uri);
        // 参数值
        Object[] args = pjp.getArgs();
        // 参数名
        String[] argNames = ((MethodSignature) pjp.getSignature()).getParameterNames();
        if (args != null && argNames != null) {
            Map<String, Object> nameAndValue = new HashMap<>(5);
            for (int i = 0; i < argNames.length; i++) {
                if (!(args[i] instanceof HttpServletRequest) && !(args[i] instanceof HttpServletResponse)) {
                    nameAndValue.put(argNames[i], JSONObject.toJSON(args[i]));
                }
            }
            logger.info("参数列表：-->" + nameAndValue);
        }
        Map<String, String[]> params = request.getParameterMap();
        Map<String, Object> requestParam = new HashMap(10);
        for (String param : params.keySet()) {
            requestParam.put(param, JSONObject.toJSON(params.get(param)));
        }
        logger.info("request参数列表：-->" + requestParam);
    }

    */
/**
     * 全局异常处理
     *
     * @param response
     * @param e
     * @throws Exception
     *//*

    private void exceptionHandler(HttpServletResponse response, Throwable e) throws Throwable {
        if (e instanceof BizException) {
            throw e;
        }
    }
}
*/
