package com.gds.common;

/**
 * 数据状态
 */
public enum StatusEnum {
    N(10, "正常"),
    D(30, "删除");

    private int code;
    private String name;

    private StatusEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
