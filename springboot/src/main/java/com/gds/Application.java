package com.gds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Created by crj on 2017/11/6.
 */
@SpringBootApplication
@EnableAsync
public class Application {
    @Bean
    public HttpMessageConverters fastJsonHttpMessageConverters() {
        // 1、需要先定义一个converter 转换器
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        // 2、添加fastJson 的配置信息，比如：是否要格式化返回的json数据
        FastJsonConfig  fastJsonConfig = new FastJsonConfig();
//        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
        //写出null值
        fastJsonConfig.setSerializerFeatures(SerializerFeature.WriteMapNullValue);
        // 3、在convert 中添加配置信息
        fastConverter.setFastJsonConfig(fastJsonConfig);
        // 4、将convert 添加到converters当中
        HttpMessageConverter<?> converter = fastConverter;
        return new HttpMessageConverters(converter);
    }
    public static void main(String[] args) {
//        SpringApplication application = new SpringApplication(Application.class);
//        Map<String, Object> defaultMap = new HashMap<String, Object>();
//
//        //这个是配置文件地址，如果打包时排除项目中的配置文件而引用外部文件的话，启动脚本必须指定此文件地址
//        defaultMap.put("base.path", "classpath:");
//
//       // 还可以是Properties对象
//        application.setDefaultProperties(defaultMap);
//
//        application.run(args);
        SpringApplication.run(Application.class,args );
    }
}
