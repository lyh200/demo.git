public class Test3 {
    public static void main(String[] args) {
        Integer a = 1;
        Integer b = 2;
        Integer c = 3;
        Integer d = 3;
        Integer e = 11;
        Integer f = 11;
        Long g = 3L;
        Long h = 2L;

        Integer i = 127;
        Integer k = 128;
        Long l = 128L;
        System.out.println(c == d); //true   Integer 缓冲池   -128-127
        System.out.println(e == f); //true  Integer 缓冲池   -128-127  e、f 为同一个对象
        System.out.println(c == (a + b)); //true  //Integer 相加 为 value的相加
        System.out.println(k == (a + i)); //true  //Integer 相加 为 value的相加
        System.out.println(c.equals(a + b)); //true
        System.out.println(g == (a + b)); //true
        System.out.println(g.equals(a + b)); //false
        System.out.println(g.equals(a + h));//true
        System.out.println(l.equals(k));//false

        Integer a1 = new Integer(1);
        Integer b1 = new Integer(1);
        int m = 1;
        int n = 1;
        long u = 1l;

        Integer q = 300;
        Integer p = 300;

        System.out.println("----" + (a1 == b1));  //这是两个对象
        System.out.println("----" + (m == n));  //这是两个对象
        System.out.println("----" + (m == u));  //这是两个对象
        System.out.println("----" + (q == p));  //这是两个对象

    }
}
