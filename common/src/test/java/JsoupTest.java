import com.alibaba.fastjson.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsoupTest {

    public static void  main(String[] args) throws IOException {
        List<Shop>  shops =  new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
          shops.addAll(get58Data(i));
        }

        toPrint(shops);
    }

    public static void toPrint(List<Shop> shops){
        Map map =  new HashMap();
        map.put("shops",shops);
        JSONObject jsonObject  = (JSONObject) JSONObject.toJSON(map);
        System.out.println(jsonObject.toJSONString());
    }

    public static List<Shop> getGanjiData(Integer page){

        return  null;
    }
    public static  List<Shop>  get58Data(Integer page) {
        Document doc = null;
        try {
            System.out.println("正在爬取第" + page + "页");
            doc = Jsoup.connect("http://sz.58.com/meirongshi/pn" + page).data("ClickID", page.toString())
                    .userAgent("Mozilla")
                    .cookie("auth", "token")
                    .timeout(3000)
                    .get();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("连接异常！！！！！！！！！！！！！！！！");
        }

        List<Shop>  shops =  new ArrayList<>();
//        System.out.println(doc);
        Elements li = doc.select("li[class=job_item clearfix]");
        System.out.println("拿到数据条数----：" + li.size());
        System.out.println("正在解析数据");
        for (Element lie : li) {
            System.out.println("-----------------------");
            String href = lie.select("a[target=_blank]").attr("href");
//            System.out.println("网址---" + href);
            String shopName = lie.select("a[class=fl]").attr("title");
            System.out.println("店铺名称___" + shopName);
            String address = lie.select("span[class=address]").html();
//            System.out.println("店铺地址——" + address);
            String cate = lie.select("span[class=cate]").html();
//            System.out.println("职位——" + cate);
            String daiyu = lie.select("span[class=name]").html();
//            System.out.println("待遇——" + daiyu);
            System.out.println("-----------------------");
            Shop shop = new Shop();
            shop.setJob(cate);
            shop.setName(shopName);
            shop.setUrl(href);
            shop.setWelfare(daiyu);
            shop.setAddress(address);
            shops.add(shop);
        }
        System.out.println("解析完毕");
        return shops;
    }
}
