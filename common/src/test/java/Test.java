import validation.ValidationResult;
import validation.ValidationUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static java.lang.Thread.sleep;

public class Test {

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(1000);
        Map map = new HashMap<>();
        ReadWriteLock lock = new ReentrantReadWriteLock();

        Runnable writeTask = () -> {
            lock.writeLock().lock();
            try {
                sleep(2000);
                map.put("foo", "bar");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.writeLock().unlock();
            }
        };
        Runnable readTask = () -> {
            lock.readLock().lock();
            try {
                System.out.println(map.get("foo"));
                sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.readLock().unlock();
            }
        };


      for(int i=0;i<1000;i++) {
          executor.submit(readTask);
      }
        executor.submit(writeTask);

      executor.shutdown();
    }


}
