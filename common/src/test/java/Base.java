public class Base {
    public String baseName = "base";
    public Base() {
        callName();
    }

    public void callName() {
        System.out.println("base"+baseName);

    }
    public static void main(String[] args) {
        Sub b = new Sub();
        System.out.println(b);
    }
}

class Sub extends Base {
    private String baseName = "sub";

    public void callName(){
        System.out.println("sub:"+baseName);
    }
}
