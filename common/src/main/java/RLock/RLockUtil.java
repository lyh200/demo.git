package RLock;


import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;

import java.util.concurrent.TimeUnit;

@Slf4j
public class RLockUtil {

    /****
     * RLock 封装
     * @param lock      RLock
     * @param waitTime  锁等待时间
     * @param leaseTime 持有时间
     * @param unit      时间单位
     * @param businessKey  业务key
     * @param businessKey  业务值
     * @param doLocked     锁定业务处理
     * @param doFailedLock 锁定失败业务处理
     */
    public static void lock(RLock lock,
                            long waitTime,
                            long leaseTime,
                            TimeUnit unit,
                            String businessKey,
                            String businessValue,
                            DoLocked doLocked,
                            DoFailedLock doFailedLock) {
        try {
            if (lock.tryLock(waitTime, leaseTime, unit)) {
                doLocked.doBusiness(businessKey);
            } else {
                doFailedLock.DoLockFail(businessKey);
            }
        } catch (InterruptedException e) {
            log.error("{} businessKey:{} businessValue:{} exception:{} ", businessKey, businessValue, e.getMessage());
        } finally {
            if (lock.isLocked() && lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
        }

    }


//    public static void lock(
//            String businessKey,
//            DoLocked doLockBusiness,
//            DoFailedLock fail) throws WxErrorException {
//
//        doLockBusiness.doBusiness(businessKey);
//        fail.DoLockFail(businessKey);
//
//
//    }
//
//    public static void main(String[] args) throws WxErrorException {
//        RLockUtil.lock("cmkj",
//                (key) -> {System.out.println("doLocked:" + key + " ");},
//                (key) -> {}
//        );
//    }

}


