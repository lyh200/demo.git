package RLock;

@FunctionalInterface
public interface DoLocked {

    void doBusiness(String key) ;

}

