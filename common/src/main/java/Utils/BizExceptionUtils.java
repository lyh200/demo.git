package Utils;

import Utils.entity.BizException;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Collection;

public class BizExceptionUtils {

    /***
     * 业务抛出异常
     * @param object 判断对象
     * @param tip  提示语
     * @param isString string 判空
     */
    public static void isEmpty(Object object, String tip, boolean isString) {
        isEmpty(object, null, tip, isString);
    }
    public static void collectionIsEmpty(Collection<?> collection,String tip){
        if(CollectionUtils.isEmpty(collection)){
            bizException(tip);
        }
    }

    /***
     * 根据conditon  == true 抛异常
     * @param condition
     * @param tip
     */
    public static void isTrue(Boolean condition, String tip) {
        if (condition) {
            bizException(tip);
        }
    }

    public static void isTrue(Boolean condition, String errCode, String tip) {
        if (condition) {
            bizException(errCode, tip);
        }
    }

    /***
     * 业务抛出异常 可以自定义errno
     * @param object
     * @param errno
     * @param tip
     * @param isString
     */
    public static void isEmpty(Object object, String errno, String tip, boolean isString) {
        if (object == null) {
            bizException(errno, tip);
        }
        if (isString) {
            if (StringUtils.isEmpty(isString)) {
                bizException(errno, tip);
            }
        }
    }

    public static void bizException(String errno, String tip) {
        throw new BizException(errno, tip);
    }

    public  static void bizException(String tip) {
        throw new BizException(tip);
    }

}
