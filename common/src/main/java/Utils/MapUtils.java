package Utils;


import org.springframework.util.StringUtils;

import java.util.Map;

public class MapUtils {
    /***
     * map放入不为空的value
     * @param targetMap 目标map
     * @param key       key
     * @param value     value
     * @param isString  是不是字符串
     */
    public static void putMap(Map<String, Object> targetMap, String key, Object value, Boolean isString) {
        boolean isFlage = true;
        if (value == null) {
            isFlage = false;
        }
        if (isString || isFlage) {
            if(StringUtils.isEmpty(value)){
                isFlage = false;
            }
        }
        if (isFlage) {
            targetMap.put(key, value);
        }
    }
}