package Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


public class Beans {

    private static final Logger logger = LoggerFactory.getLogger(Beans.class);

    /**
     * 把具有相同字段和类型的bean进行转换
     * @param source
     * @param targetClass
     * @param <T>
     * @return
     */
    public static <T> T convert(Object source, Class<T> targetClass) {
        T target = null;

        if(source != null) {
            try {
                target = targetClass.newInstance();
                BeanUtils.copyProperties(source, target);
            }catch (Exception e) {
                logger.error("convert bean is error",e);
            }

        }

        return target;
    }

    public static <T> List<T> convertList(List sourceList, Class<T> targetClass) {
        List<T> targetList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(sourceList)) {
            sourceList.forEach(source -> {
                targetList.add(convert(source, targetClass));
            });
        }
        return targetList;
    }
    public static <T> T convert(Object source, Class<T> targetClass , String[] ignoreProperties) {
        T target = null;

        if(source != null) {
            try {
                target = targetClass.newInstance();
                BeanUtils.copyProperties(source, target,ignoreProperties);
            }catch (Exception e) {
                logger.error("convert bean is error",e);
            }

        }

        return target;
    }

    public static void setFieldValue(Object object, String fieldName, Object value) {
        Field field = ReflectionUtils.findField(object.getClass(), fieldName);
        if (field == null) {
            throw new IllegalArgumentException("Could not find field [" + fieldName + "] on target [" + object + "]");
        }
        ReflectionUtils.makeAccessible(field);
        ReflectionUtils.setField(field, object, value);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getFieldValue(Object object, String fieldName) {
        Field field = ReflectionUtils.findField(object.getClass(), fieldName);
        if (field == null) {
            throw new IllegalArgumentException("Could not find field [" + fieldName + "] on target [" + object + "]");
        }
        ReflectionUtils.makeAccessible(field);
        return (T) ReflectionUtils.getField(field, object);
    }
}
