package Utils;

import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public class ModelUtils {
    // 数据库正常状态
    public static final int NORMAL_STATUS = 0;
    // 数据库删除状态
    public static final int DELETE_STATUS = -1;

    public static final String DEFAULT_OPERATOR = "10000";

    // 根据项目约定，该操作的表都需要对应 creator／lastModifier／gmtCreate /gmtModify / status 字段

    public static void initBean(Object object) {
        initBean(object, getOperator());
    }

    private static String getOperator() {
        String operator =  UserContext.getUserHolder() == null ? DEFAULT_OPERATOR:UserContext.getUserHolder().getRealName();
        return  StringUtils.isEmpty(operator) ? DEFAULT_OPERATOR: operator;
    }

    public static void initBean(Object object, String operator) {
        if (object == null) {
            return;
        }

        if(StringUtils.isEmpty(operator)){
            initBean(object);
            return;
        }
        LocalDateTime now = LocalDateTime.now();
        initBean(object,operator,operator,now,now);
    }

    private static void initBean(Object object, String createUser, String modifyUser, LocalDateTime createDate, LocalDateTime lastModifyDate) {
        Field creator = ReflectionUtils.findField(object.getClass(), "creator");


        if (creator != null) {
            ReflectionUtils.makeAccessible(creator);
            ReflectionUtils.setField(creator, object, createUser);
        }

        Field lastModifier = ReflectionUtils.findField(object.getClass(), "lastModifier");


        if (lastModifier != null) {
            ReflectionUtils.makeAccessible(lastModifier);
            ReflectionUtils.setField(lastModifier, object, modifyUser);
        }

        // 兼容字段错误
        Field lastModifer = ReflectionUtils.findField(object.getClass(), "lastModifer");

        if(lastModifer != null) {
            ReflectionUtils.makeAccessible(lastModifer);
            ReflectionUtils.setField(lastModifer, object, modifyUser);
        }

        Field gmtCreate = ReflectionUtils.findField(object.getClass(), "gmtCreate");



        if (gmtCreate != null) {
            ReflectionUtils.makeAccessible(gmtCreate);
            ReflectionUtils.setField(gmtCreate, object, createDate);
        }

        Field gmtModify = ReflectionUtils.findField(object.getClass(), "gmtModify");

        if (gmtModify != null) {
            ReflectionUtils.makeAccessible(gmtModify);
            ReflectionUtils.setField(gmtModify, object, lastModifyDate);
        }

        Field createTime = ReflectionUtils.findField(object.getClass(), "createTime");


        if (createTime != null) {
            ReflectionUtils.makeAccessible(createTime);
            ReflectionUtils.setField(createTime, object, createDate);
        }

        Field lastModifyTime = ReflectionUtils.findField(object.getClass(), "lastModifyTime");


        if (lastModifyTime != null) {
            ReflectionUtils.makeAccessible(lastModifyTime);
            ReflectionUtils.setField(lastModifyTime, object, lastModifyDate);
        }


        Field status = ReflectionUtils.findField(object.getClass(), "status");


        if (status != null) {
            ReflectionUtils.makeAccessible(status);
            ReflectionUtils.setField(status, object, NORMAL_STATUS);
        }
    }
    public static void updateBean(Object object) {
        updateBean(object, getOperator());
    }

    public static void updateBean(Object object, String operator) {
        if (object == null) {
            return;
        }

        if (StringUtils.isEmpty(operator)){
            updateBean(object);
            return;
        }

        Field lastModifier = ReflectionUtils.findField(object.getClass(), "lastModifier");


        if (lastModifier != null) {
            ReflectionUtils.makeAccessible(lastModifier);
            ReflectionUtils.setField(lastModifier, object, operator);
        }

        // 兼容字段错误
        Field lastModifer = ReflectionUtils.findField(object.getClass(), "lastModifer");

        if(lastModifer != null) {
            ReflectionUtils.makeAccessible(lastModifer);
            ReflectionUtils.setField(lastModifer, object, operator);
        }

        LocalDateTime now = LocalDateTime.now();

        Field gmtModify = ReflectionUtils.findField(object.getClass(), "gmtModify");


        if (gmtModify != null) {
            ReflectionUtils.makeAccessible(gmtModify);
            ReflectionUtils.setField(gmtModify, object, now);
        }


        Field lastModifyTime = ReflectionUtils.findField(object.getClass(), "lastModifyTime");


        if (lastModifyTime != null) {
            ReflectionUtils.makeAccessible(lastModifyTime);
            ReflectionUtils.setField(lastModifyTime, object, now);
        }

    }

    public static void deleteBean(Object object, String operator) {
        updateBean(object,operator);

        Field status = ReflectionUtils.findField(object.getClass(),"status");

        if(status != null) {
            ReflectionUtils.makeAccessible(status);
            ReflectionUtils.setField(status, object, DELETE_STATUS);
        }

        Field uniqIndexLock = ReflectionUtils.findField(object.getClass(), "uniqIndexLock");
        if (uniqIndexLock != null) {
            ReflectionUtils.makeAccessible(uniqIndexLock);
            ReflectionUtils.setField(uniqIndexLock, object, System.currentTimeMillis());
        }
    }

    private static void getFiledValueList(List<String> userIds, String filedName, Object object) {
        Object value = getFiledValue(filedName, object);
        if (value != null) {
            String valueStr = value + "";
            if (!"".equals(valueStr) && !userIds.contains(valueStr)) {
                userIds.add(value + "");
            }
        }
    }


    private static void setValue(Map<String, String> userInfos, String creatorName, String creator, Object object) {
        Object value = getFiledValue(creator, object);
        if (value != null) {
            if (userInfos.containsKey(value + "")) {
                Field filed = ReflectionUtils.findField(object.getClass(), creatorName);
                if (filed != null) {
                    ReflectionUtils.makeAccessible(filed);
                    ReflectionUtils.setField(filed, object, userInfos.get(value + ""));
                }


            }
        }
    }

    private static Object getFiledValue(String filedName, Object object) {
        Field field = ReflectionUtils.findField(object.getClass(), filedName);
        if (field != null) {
            ReflectionUtils.makeAccessible(field);
            try {
                Object value = field.get(object);
                if (value != null) {
                    return value;
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void deleteBean(Object object) {
        deleteBean(object,DEFAULT_OPERATOR);
    }

}
