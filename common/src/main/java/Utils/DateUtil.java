package Utils;


import Utils.entity.FirstAndLastDay;

import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtil {



    /**
     * 英文全称 如：2014-12-01 23:15:06
     */
    public static String FORMAT_LONG = "yyyy-MM-dd HH:mm:ss";

    /**
     * 精确到毫秒的完整时间 如：yyyy-MM-dd HH:mm:ss.S
     */
    public static String FORMAT_FULL = "yyyy-MM-dd HH:mm:ss.S";

    /**
     * 中文简写 如：2014年12月01日
     */
    public static String FORMAT_SHORT_CN = "yyyy年MM月dd";

    /**
     * 中文全称 如：2014年12月01日 23时15分06秒
     */
    public static String FORMAT_LONG_CN = "yyyy年MM月dd日  HH时mm分ss秒";

    /**
     * 精确到毫秒的完整中文时间
     */
    public static String FORMAT_FULL_CN = "yyyy年MM月dd日  HH时mm分ss秒SSS毫秒";

    /**
     * 一天的毫秒数
     */
    public static final long millisecond_of_day = 1000 * 24 * 60 * 60;

    /**
     * 连连风险参数用
     */
    public static String FORMAT_LONG_24 = "yyyyMMddHHmmss";
    public static String FORMAT_LONG_SSS = "yyyyMMddHHmmssSSS";

    public static String FORMAT_LONG_YMD = "yyyyMMdd";

    public static String now(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date());
    }


    /****
     * 时间格式化
     * @param date  时间
     * @param format 格式 如 YYYY-MM-DD
     * @return
     */
    public static String format(Date date, String format) {

        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }



    public static Date parse(String dateStr, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            return sdf.parse(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date addByCalendar(Date date, int step, int calendarField) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(calendarField, step);
        return c.getTime();
    }



    // 判读是否是今天
    public static boolean isToday(Date startDate) {
        Calendar today = Calendar.getInstance();
        if (format(startDate, "yyyyMMdd").equals(format(today.getTime(), "yyyyMMdd"))) {
            return true;
        }
        return false;
    }


    /***
     * 获取昨日时间
     */
    public static String getYesterday() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String yesterday = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
        return yesterday;
    }


    /***
     * 获取某年某月有多少天
     * @param monthStr
     * @return
     */
    public static Integer getMonthDayTotal(String monthStr) {

        Integer year = null;
        Integer month = null;
        String[] time = monthStr.split("-");
        try {
            year = Integer.valueOf(time[0]);
            month = Integer.valueOf(time[1]);

        } catch (Exception e) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.set(year, month, 0); //输入类型为int类型
        return c.get(Calendar.DAY_OF_MONTH);
    }

    public static String getDateToStr(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        //获取String类型的时间
        String createdate = sdf.format(date);
        return createdate;
    }

    public static String getDateToStrYYYYMMMDDHHMMSS(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
        //获取String类型的时间
        String createdate = sdf.format(date);
        return createdate;
    }

    /***
     * 获取过去(未来)几天日期  yyy-MM-dd
     * @param postDay   负整数 过去几天  正整数 未来
     * @return
     */
    public static List<String> getPastDateStr(int postDay) {
        int num = Math.abs(postDay);
        List<String> dayList = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            //取时间
            Date date = new Date();
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            //把日期往后增加一天.整数往后推,负数往前移动
            calendar.add(Calendar.DATE, postDay);
            //这个时间就是日期往后推一天的结果
            date = calendar.getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = formatter.format(date);
            dayList.add(dateString);
            if (postDay > 0) {
                postDay--;
            }
            if (postDay < 0) {
                postDay++;
            }
        }
        return dayList;
    }

    /***
     *获取几天前(后)时刻
     *  @param date  现在时间
     * @param day  -3 三天前   3三天后
     * @return
     */
    public static Date getseveralDayAgo(Date date, Integer day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, day);
        return calendar.getTime();
    }

    public static String getseveralDayAgoStr(Date date, Integer day) {
        return getDateToStrYYYYMMMDDHHMMSS(getseveralDayAgo(date, day));
    }

    /**
     * 获取指定年月的第一天
     *
     * @param year
     * @param month
     * @return
     */
    public static String getFirstDayOfMonth(int year, int month) {
        Calendar cal = Calendar.getInstance();
        //设置年份
        cal.set(Calendar.YEAR, year);
        //设置月份
        cal.set(Calendar.MONTH, month - 1);
        //获取某月最小天数
        int firstDay = cal.getMinimum(Calendar.DATE);
        //设置日历中月份的最小天数
        cal.set(Calendar.DAY_OF_MONTH, firstDay);
        //格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(cal.getTime());
    }


    /**
     * 获取指定年月的最后一天
     *
     * @param year
     * @param month
     * @return
     */
    public static String getLastDayOfMonth(int year, int month) {
        Calendar cal = Calendar.getInstance();
        //设置年份
        cal.set(Calendar.YEAR, year);
        //设置月份
        cal.set(Calendar.MONTH, month - 1);
        //获取某月最大天数
        int lastDay = cal.getActualMaximum(Calendar.DATE);
        //设置日历中月份的最大天数
        cal.set(Calendar.DAY_OF_MONTH, lastDay);
        //格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(cal.getTime());
    }

    /***
     * 根据月份获取当月第一天 最后一天
     * @param monthStr yyyy-mm
     * @return
     */
    public static FirstAndLastDay getFirstAndLastDay(String monthStr) {
        try {
            String[] str = monthStr.split("-");
            if (str == null || str.length < 2) {
                return null;
            }
            Integer year = Integer.valueOf(str[0]);
            Integer month = Integer.valueOf(str[1]);

            FirstAndLastDay firstAndLastDay = new FirstAndLastDay();
            String first = getFirstDayOfMonth(year, month);
            String last = getLastDayOfMonth(year, month);
            firstAndLastDay.setFirst(first);
            firstAndLastDay.setLast(last);
            return firstAndLastDay;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /***
     * 获取当前月份
     * @return  yyyy-mm
     */
    public static String getCurrentMonth() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH )+1;
        String monthStr = month+"";
        if(month<10){
            monthStr ="0"+month;
        }
        return year+"-" +monthStr;
    }
}
