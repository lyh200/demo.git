package Utils;

import lombok.Data;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


public class UserContext {

    private static final String ADMIN_BOSS_API_URL = "/admin/";

    public static final List<String> IGNORE_URI = new ArrayList() {{
        add("/door/");

    }};

    /**
     * 登录用户上下文存储
     */
    private static final ThreadLocal<UserHolder> context = new ThreadLocal<>();

    @Data
    public static class UserHolder {

        private String userId;

        private String wxWorkUserId;

        private String schoolId;

        private String mobile;

        private String token;

        private String realName;

        private String ip;
    }

    public static void clear() {
        UserContext.context.remove();
    }

    public static void addUserContext(UserHolder userHolder) {
        context.set(userHolder);
    }

    public static UserHolder getUserHolder() {
        return context.get();
    }

    public static boolean isWhiteUrl(HttpServletRequest request) {
        String url = request.getRequestURL().toString();
        return IGNORE_URI.stream().anyMatch(s -> url.contains(s));

    }

    public static boolean isAdminUrl(HttpServletRequest request) {
        String url = request.getRequestURI();
        return url.startsWith(UserContext.ADMIN_BOSS_API_URL);

    }

}

