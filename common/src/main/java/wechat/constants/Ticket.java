package wechat.constants;

import lombok.Data;

@Data
public class Ticket {

    public   String  value;

    /***
     * 过期时间毫秒数
     */
    private  Long   overdue;

    /***
     * 有效时间 秒
     */
    private  Long   expire;
}
