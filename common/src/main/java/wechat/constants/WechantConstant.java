package wechat.constants;

public class WechantConstant {

    public volatile static   Ticket  accessToken;
    public volatile static   Ticket  ticket;


    /***域名 公众号配置域名*******/
    public final  static  String hostUrl="http://XXXXXXXXXXXXXXXX";
    /***支付页面目录***/
    public final static  String redirect="";
    public final static String  appId="公众号appid";
    public final static String secret="微信公众号秘钥";
    public final static String apiKey="支付apikey";
    public final static String mchId ="支付商户码";
    public final static String notifyUrl ="支付回调函数路径";
    public final static String token="token";
}
