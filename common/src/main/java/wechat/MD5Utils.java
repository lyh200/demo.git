package wechat;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

import java.security.MessageDigest;


public class MD5Utils {

	private static final String	salt	= "HYGL";

	public static String encrypt(String str) {
		String encodePwd = null;
		if (str != null) {
			Md5PasswordEncoder passwdEncoder = new Md5PasswordEncoder();
			encodePwd = passwdEncoder.encodePassword(str, salt);
		}
		return encodePwd;
	}
	
	public static String encrypt(String str, boolean isOldUser) {
		if(isOldUser){
			 XLMd5Utils md5 = new XLMd5Utils();
			 return md5.getMD5ofStr(str);
		}else{
			return encrypt(str);
		}
	}

	public static String md5(String str) {
		try {
			byte[] btInput = str.getBytes("UTF-8");
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			mdInst.update(btInput);
			byte[] md = mdInst.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < md.length; i++) {
				int val = ((int) md[i]) & 0xff;
				if (val < 16) {
					sb.append("0");
				}
				sb.append(Integer.toHexString(val));
			}
			return sb.toString();
		} catch (Exception e) {
			return null;
		}
	}
	
	public static void main(String[] args) {
		/*System.out.println(MD5Utils.encrypt("123456", false));
		System.out.println(MD5Utils.encrypt("111111", false));
		System.out.println(MD5Utils.encrypt("123456", false));
		System.out.println(MD5Utils.encrypt("wx211115", true));
		System.out.println("wcw:" + MD5Utils.encrypt("2007144235wdwcw", true));
		System.out.println(MD5Utils.encrypt("lt111111", false));
		String pwd = MD5Utils.encrypt("lt111111",true);
		if(pwd.equals("CDB50BD4E895D878964820DFA50E8701")){
			System.out.println("found pwd");
		}*/
//		ab8542d041cd5c13c62a86045e42f34b
//		CDB50BD4E895D878964820DFA50E8701
		System.out.println(md5("0000002t66e0b8a9c152574a8228b12456aohb246l1"));
	}
}
