package wechat;

import java.util.regex.Pattern;

public class PwdStrengthUtil {
	public static Integer getPwdStrength(String password) {
		if (Pattern.matches("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", password)) {
			return 3;
		} else if (Pattern.matches("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", password)) {
			return 2;
		}
		// 弱 ^(?:\d+|[a-zA-Z]+|[!@#$%^&*]+)$
		return 1;
	}
}
