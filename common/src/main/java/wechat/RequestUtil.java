package wechat;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;

/**
 * <pre>
 * 统一获取用户IP的代码入口
 * </pre>
 */
public final class RequestUtil {
    private static Log log = LogFactory.getLog(RequestUtil.class);

    public static HttpServletRequest getRequest() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return request;
    }

    public static HttpSession getSession() {
        HttpServletRequest request = getRequest();
        if (request != null) {
            return request.getSession();
        }
        return null;
    }


    public static String getRemoteAddr(HttpServletRequest request) {
        if (request != null) {
            String ip = request.getHeader("X-Forwarded-For");
            if (StringUtils.isBlank(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
            }
            if (StringUtils.isBlank(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }
            if (StringUtils.isBlank(ip)) {
                ip = request.getRemoteAddr();
            }
            if (!StringUtils.isNotBlank(ip) && ip.contains(",")) {
                String[] ips = ip.split(",");
                ip = ips[ips.length - 1];
            }

            if (!ValidUtils.isIP4(ip)) {
                ip = "0.0.0.0";
            }

            if (StringUtils.isNotBlank(ip))
                return ip;
            else {
                Enumeration<String> keys = request.getHeaderNames();
                while (keys.hasMoreElements()) {
                    String key = keys.nextElement();
                    log.info(key + " = " + request.getHeader(key));
                }
            }
        }
        return "";
    }

    public static String getRemoteAddr() {
        HttpServletRequest request = getRequest();
        return getRemoteAddr(request);
    }


    /**
     * 获取用户真实IP地址，不使用request.getRemoteAddr()的原因是有可能用户使用了代理软件方式避免真实IP地址,
     * 可是，如果通过了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP值
     *
     * @return ip
     */
    public static String getIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

}
