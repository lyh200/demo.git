
package wechat;

import java.util.HashMap;
import java.util.Map;

/**
 * Map工具类.
 */
public class MapUtil {

	/**
	 * 接口调用返回代码
	 */
	public static final String INTF_SUC_CODE = "200";
	public static final String INTF_ERR_CODE = "400";
	/**
	 * 登录名为空
	 */
	public static final String INTF_ERR_CODE_401 = "401";
	/**
	 * 密码为空
	 */
	public static final String INTF_ERR_CODE_402 = "402";
	/**
	 * 用户名或密码错误
	 */
	public static final String INTF_ERR_CODE_403 = "403";
	/**
	 * 手机号格式不正确
	 */
	public static final String INTF_ERR_CODE_404 = "404";
	/**
	 * 验证码过期
	 */
	public static final String INTF_ERR_CODE_405 = "405";
	/**
	 * 密码长度不符
	 */
	public static final String INTF_ERR_CODE_406 = "406";
	/**
	 * 手机号已注册
	 */
	public static final String INTF_ERR_CODE_407 = "407";
	/**
	 * 验证码不正确
	 */
	public static final String INTF_ERR_CODE_408 = "408";
	/**
	 * 邀请码不正确
	 */
	public static final String INTF_ERR_CODE_409 = "409";
	/**
	 * 手机号不存在
	 */
	public static final String INTF_ERR_CODE_410 = "410";
	/**
	 * 新密码为空
	 */
	public static final String INTF_ERR_CODE_411 = "411";
	/**
	 * 确认新密码为空
	 */
	public static final String INTF_ERR_CODE_412 = "412";
	/**
	 * 新密码与确认新密码不相同
	 */
	public static final String INTF_ERR_CODE_413 = "413";
	/**
	 * 用户被锁定
	 */
	public static final String INTF_ERR_CODE_414 = "414";
	/**
	 * 参数有误
	 */
	public static final String INTF_ERR_CODE_415 = "415";
	/**
	 * 会话过期
	 */
	public static final String INTF_ERR_CODE_416 = "416";
	/**
	 * 未设置交易密码
	 */
	public static final String INTF_ERR_CODE_417 = "417";
	/**
	 * 交易密码错误
	 */
	public static final String INTF_ERR_CODE_418 = "418";
	/**
	 * 股票代号为空
	 */
	public static final String INTF_ERR_CODE_419 = "419";
	/**
	 * 开始时间为空
	 */
	public static final String INTF_ERR_CODE_420 = "420";
	/**
	 * 结束时间为空
	 */
	public static final String INTF_ERR_CODE_421 = "421";
	/**
	 * 数据不存在
	 */
	public static final String INTF_ERR_CODE_422 = "422";
	/**
	 * 非法用户
	 */
	public static final String INTF_ERR_CODE_423 = "423";
	/**
	 * 已经结算,不能再次结算
	 */
	public static final String INTF_ERR_CODE_424 = "424";
	/**
	 * 商户码为空
	 */
	public static final String INTF_ERR_CODE_425 = "425";
	/**
	 * 商户码错误
	 */
	public static final String INTF_ERR_CODE_426 = "426";
	/**
	 * 用户名长度不符
	 */
	public static final String INTF_ERR_CODE_427 = "427";
	/**
	 * 邮箱为空
	 */
	public static final String INTF_ERR_CODE_428 = "428";
	/**
	 * 邮箱格式不正确
	 */
	public static final String INTF_ERR_CODE_429 = "429";
	/**
	 * 收货人为空
	 */
	public static final String INTF_ERR_CODE_430 = "430";
	/**
	 * 联系方式为空
	 */
	public static final String INTF_ERR_CODE_431 = "431";
	/**
	 * 地区为空
	 */
	public static final String INTF_ERR_CODE_432 = "432";
	/**
	 * 地址为空
	 */
	public static final String INTF_ERR_CODE_433 = "433";
	/**
	 * 商品已下架
	 */
	public static final String INTF_ERR_CODE_434 = "434";
	/**
	 * 商品库存不足
	 */
	public static final String INTF_ERR_CODE_435 = "435";
	/**
	 * 余额不足
	 */
	public static final String INTF_ERR_CODE_436 = "436";
	
	/**
	 * 封装公共返回Map.
	 */
	public static Map<String, Object> getRetMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("errno", INTF_SUC_CODE);	//默认返回码 200
		map.put("msg", "返回成功");	//默认返回信息 调用成功
		return map;
	}

}
