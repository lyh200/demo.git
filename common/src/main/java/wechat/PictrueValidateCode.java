package wechat;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

public class PictrueValidateCode {

	public static final String	PIC_CODE_KEY	= "pic-validate-code";						// 放到session中的key

	private static String		randString		= "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";	// 随机产生的字符串

	private static int			lineSize		= 41;										// 干扰线数量

	private static int			stringNum		= 4;										// 随机产生字符数量

	/**
	 * 字体大小
	 */
	private static int 			charactorSize = 33;
	/*
	 * 获得字体
	 */
	private static Font getFont() {
		return new Font("Fixedsys", Font.CENTER_BASELINE, charactorSize);
	}

	/*
	 * 获得颜色
	 */
	private static Color getRandColor(int fc, int bc) {
		Random random = new Random();

		if (fc > 255)
			fc = 255;
		if (bc > 255)
			bc = 255;
		int r = fc + random.nextInt(bc - fc - 16);
		int g = fc + random.nextInt(bc - fc - 14);
		int b = fc + random.nextInt(bc - fc - 18);
		return new Color(r, g, b);
	}

	/*
	 * private static Color getRandColor(int fc,int bc){ Random random = new
	 * Random();
	 * 
	 * if(fc > 255) fc = 255; if(bc > 255) bc = 255; int r =
	 * random.nextInt(255); int g = random.nextInt(255); int b =
	 * random.nextInt(255); return new Color(r,g,b); }
	 */

	/**
	 * 提供验证码图片
	 * 
	 * @param sessionId
	 * @param out
	 */
	public static String generatePictrue(OutputStream out, int width, int height, String fontName) {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
		Graphics g = image.getGraphics();// 产生Image对象的Graphics对象,改对象可以在图像上进行各种绘制操作
		g.setColor(getRandColor(210, 253));
		g.fillRect(0, 0, width, height);
		g.setColor(getRandColor(110, 133));
		// 绘制干扰线
		for (int i = 0; i <= lineSize; i++) {
			drowLine(g, width, height);
		}
		// 绘制随机字符
		StringBuffer randomString = new StringBuffer();
		for (int i = 0; i < stringNum; i++) {
			randomString.append(drowString(g, i, width, height, new Font(fontName, Font.CENTER_BASELINE, charactorSize)));
		}

		g.dispose();
		try {
			ImageIO.write(image, "JPEG", out);// 将内存中的图片通过流动形式输出到客户端
		} catch (Exception e) {
			e.printStackTrace();
		}
		return randomString.toString();
	}

	private static String drowString(Graphics g, int i, int width, int height, Font font) {
		Random random = new Random();
		g.setFont(font);
		g.setColor(new Color(random.nextInt(101), random.nextInt(111), random.nextInt(121)));
		String rand = String.valueOf(getRandomString(random.nextInt(randString.length())));
		//g.translate(random.nextInt(3), random.nextInt(3));
		//g.drawString(rand, (int)(font.getSize()*0.7*i)+random.nextInt(10), height-(height-font.getSize())/2);
		g.drawString(rand, width/stringNum*i+random.nextInt(12), height-(height-font.getSize())/2+(random.nextInt(7)-3));
		return rand;
	}

	/*
	 * 绘制干扰线
	 */
	private static void drowLine(Graphics g, int width, int height) {
		Random random = new Random();
		int x = random.nextInt(width);
		int y = random.nextInt(height);
		int xl = random.nextInt(13);
		int yl = random.nextInt(15);
		g.drawLine(x, y, x + xl, y + yl);
	}

	/*
	 * 获取随机的字符
	 */
	public static String getRandomString(int num) {
		return String.valueOf(randString.charAt(num));
	}

	public static void main(String[] args) throws IOException {
		String[] fonts = getAllFont();
		for (int i = 0; i < 1; i++) {
			OutputStream out = new FileOutputStream("e:/test/" + fonts[i] + ".png");
			generateWithAllFonts(90, 60, out, getFont());
		} 
/*		for (int i = 0; i < 1; i++) {
			Date d = new Date();
			OutputStream out = new FileOutputStream("e:/test/" + fonts[i] + ".png");
			Font font = new Font(fonts[i], Font.CENTER_BASELINE, 18);
			generateWithAllFonts(80, 60, out, font);
		}*/
	}

	public static String[] getAllFont() {
		// 获取系统中可用的字体的名字
		GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
		String[] fontName = e.getAvailableFontFamilyNames();
		for (int i = 0; i < fontName.length; i++) {
			System.out.println(fontName[i]);
		}
		return fontName;
	}

	/**
	 * 用所有的字体
	 * 
	 * @return
	 */
	public static void generateWithAllFonts(int width, int height, OutputStream out, Font font) {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
		Graphics g = image.getGraphics();// 产生Image对象的Graphics对象,改对象可以在图像上进行各种绘制操作
		g.setColor(getRandColor(210, 253));
		g.fillRect(0, 0, width, height);
		g.setColor(getRandColor(110, 133));

		// 绘制随机字符
		StringBuffer randomString = new StringBuffer();
		for (int i = 0; i <  stringNum; i++) {
			randomString.append(drowString(g, i, width, height, font));
		}

		g.dispose();
		try {
			ImageIO.write(image, "png", out);// 将内存中的图片通过流动形式输出到客户端
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}