package wechat.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class BaseResult {
    private Integer code;
    private Integer errcode;
    private String errmsg;

}
