package wechat.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/***
 *
 * { "access_token":"ACCESS_TOKEN",
 * "expires_in":7200,
 * "refresh_token":"REFRESH_TOKEN",
 * "openid":"OPENID",
 * "scope":"SCOPE" }
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Oauth2AccessTokenResult extends BaseResult {
    private String access_token;
    private Long expires_in;
    private String  refresh_token;
    private String openid;
    private String scope;

}
