package wechat.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/***
 * 全局token获取
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CgiAccessTokenResult extends BaseResult {
    private String access_token;
    private Long expires_in;
}
