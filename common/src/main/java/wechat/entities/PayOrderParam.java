package wechat.entities;

import lombok.Data;

/***
 * 微信支付参数封装
 * @lyh
 */
@Data
public class PayOrderParam {
    /***
     *  微信支付分配的公众账号ID（企业号corpid即为此appId）
     * @(必填)
     * ***/
    private String appId;
    /***
     * 微信支付分配的商户号
     * @(必填)
     */
    private String mchId;
    /****
     * 商品描述 如:腾讯充值中心-QQ会员充值
     * 长度 <=128
     * @必填
     */
    private String body;

    /****
     * 商户订单号
     * 长度 <=32
     * @必填
     */
    private String outTradeNo;

    /***
     * 标价币种
     * 默认人民币 CNY
     * @必填
     */
    private String feeType="CNY";

    /***
     * 标价金额
     *  单位:分
     *  @必填
     */
    private int  totalFee;

    /***
     *终端IP
     *APP和网页支付提交用户端ip，Native支付填调用微信支付API的机器IP
     * @必填
     */
    private String spBillCreateIp ;

    /***
     * 通知地址(自己写的回调函数地址)
     * 异步接收微信支付结果通知的回调地址，通知url必须为外网可访问的url，不能携带参数。
     * @必填
     */
    private String NotifyUrl;

    /****
     * 交易类型
     *   JSAPI 公众号支付
     *   NATIVE 扫码支付
     *   APP APP支付
     * @必填
     */
    private String tradeType;

    /***
     *用户标识
     *  trade_type=JSAPI时（即公众号支付），此参数必传，此参数为微信用户在商户对应appid下的唯一标识。
     *  openid如何获取，可参考【获取openid】。企业号请使用【企业号OAuth2.0接口】获取企业号内成员userid，再调用【企业号userid转openid接口】进行转换
     * @必填
     */
    private String  openId;
}
