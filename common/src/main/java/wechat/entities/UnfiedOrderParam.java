package wechat.entities;

import lombok.Data;

@Data
public class UnfiedOrderParam {

    /***
     * 公众账号ID
     * TODO 必填
     */
    private  String  appid;
    /***
     * 商户号
     *TODO  必填
     */
    private  String  mch_id;
    /***
     * 设备号
     */
    private  String  device_info;

    /***
     * 随机字符串	nonce_str
     * TODO 必填
     */
    private String  nonce_str;

    /***
     *  签名	sign
     *  todo 必填
     */
    private  String  sign;

    /**
     * 签名类型	sign_type
     * todo 必填
     * MD5	签名类型，默认为MD5，支持HMAC-SHA256和MD5。
     */
    private String sign_type;

    /***
     * 商品描述
     * TODO 必填
     *
     */
    private String body;

    private String  out_trade_no;

    private String  fee_type;

    private int  total_fee;

    /***
     * APP和网页支付提交用户端ip，Native支付填调用微信支付API的机器IP。
     */
    private String spbill_create_ip;

    /***
     * JSAPI 公众号支付
     *
     * NATIVE 扫码支付
     *
     * APP APP支付
     */
    private String trade_type;

    private String openid;
    private String notify_url;


}
