package wechat.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CgiTicketResult extends BaseResult {

    private String ticket;
    private Long expires_in;
}
