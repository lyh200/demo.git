package wechat;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@XmlRootElement
public class JaxbUtil {
	
	
	/* public static Map<String,Object> parse(String protocolXML) {   
		 
		 Map<String,Object>   map  = new HashMap<String, Object>();
	        try {   
	             DocumentBuilderFactory factory = DocumentBuilderFactory   
	                     .newInstance();   
	             DocumentBuilder builder = factory.newDocumentBuilder();   
	             Document doc = builder   
	                     .parse(new InputSource(new StringReader(protocolXML)));   
	  
	             Element root = doc.getDocumentElement();   
	             NodeList books = root.getChildNodes();   
	             if (books != null) {   
	                for (int i = 0; i < books.getLength(); i++) {   
	                     Node book = books.item(i); 
	                     map.put(book.getNodeName(), book.getFirstChild().getNodeValue());
	                     System.out.println("节点=" + book.getNodeName() + "\ttext="  
	                             + book.getFirstChild().getNodeValue());   
	                 }   
	              }   
	         } catch (Exception e) {   
	             e.printStackTrace(); 
	             return null;
	         }   
	        
	        return map;
	     }  */ 
	
	 public static Map<String,Object> parse(String protocolXML) {   
		 Map<String,Object>   map  = new HashMap<String, Object>();
	        try {   
	  
	             Document doc=(Document)DocumentHelper.parseText(protocolXML);   
	             Element books = doc.getRootElement();   
	             Iterator   Elements = books.elementIterator();   
	             while(Elements.hasNext()){   
	                Element user = (Element)Elements.next();   
	                map.put(user.getName(), user.getText());
	             }   
	         } catch (Exception e) {   
	             e.printStackTrace(); 
	             return null;
	         }  
	       return map;
	     }   
	 
	 
	
	 
	 public static void main(String[] args) {
		 String a = "<?xml version='1.0' encoding='utf-8'?>   "
			 		+ "         <SubmitResult xmlns='http://106.ihuyi.com/'>                   "
			 		+ "<code>4072</code><msg>你提交过来的短信内容必须与报备过的模板格式相匹配"
			 		+ "</msg><smsid>0</smsid></SubmitResult>";
		 parse(a);
	}
	 
	 

	 

    /**
     * JavaBean转换成xml
     * 
     * @param obj
     * @param encoding
     * @return
     */
    public static String convertToXml(Object obj) {

        try {
            JAXBContext context = JAXBContext.newInstance(obj.getClass());
            Marshaller marshaller = context.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_ENCODING, "GBK");
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            //注意jdk版本
            XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter xmlStreamWriter = xmlOutputFactory
                    .createXMLStreamWriter(baos, (String) marshaller
                            .getProperty(Marshaller.JAXB_ENCODING));
            xmlStreamWriter.writeStartDocument(
                    (String) marshaller.getProperty(Marshaller.JAXB_ENCODING),
                    "1.0");
            marshaller.marshal(obj, xmlStreamWriter);
            xmlStreamWriter.writeEndDocument();
            xmlStreamWriter.close();
            return new String(baos.toString("GBK"));
            
        } catch (Exception e) {
            
            e.printStackTrace();
        }
        return null;

    }

    /**
     * xml转换成JavaBean
     * 
     * @param xml
     * @param c
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T converyToJavaBean(String xml, Class<T> c) {
        T t = null;
        try {
            JAXBContext context = JAXBContext.newInstance(c);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            t = (T) unmarshaller.unmarshal(new StringReader(xml));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return t;
    }

}

   