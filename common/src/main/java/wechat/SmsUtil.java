package wechat;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 发送短信的工具类
 * 
 *
 */
public class SmsUtil {

	private static Logger	log						= LoggerFactory.getLogger(SmsUtil.class);

	private String			userName				= "11";

	private String			password				= "111";

	private String			userNameZhenAu			= "11";

	private String			passwordZhenAu			= "111";

	private String			jianzhouUserName		= "111";

	private String			jianzhouPassword		= "111";
	
	
	private static String		    huyiUserName			= "cf_xlct";

	private static String			huyiPassword			= "XUj8PE";

	private String			exceptionReceiverGroup1	= "xuxunjing,litao,chenzhuo,liuze";

	public void setUserNameZhenAu(String userNameZhenAu) {
		this.userNameZhenAu = userNameZhenAu;
	}

	public void setPasswordZhenAu(String passwordZhenAu) {
		this.passwordZhenAu = passwordZhenAu;
	}

	private static final Map<Integer, String>	convertMap	= new HashMap<Integer, String>();
	static {
		convertMap.put(0, "失败");
		convertMap.put(-1, "账号错误");
		convertMap.put(-2, "密码错误");
		convertMap.put(-3, "账号已停用");
		convertMap.put(-4, "内容或号码为空");
		convertMap.put(-5, "内容包含非法关键词");
		convertMap.put(-6, "时间格式错误");
		convertMap.put(-7, "时间格式错误");
		convertMap.put(-100, "时间格式错误");
		convertMap.put(-200, "系统错误");
	}

	/**
	 * 执行一个HTTP GET请求，返回请求响应的HTML
	 * 
	 * @param url
	 *            请求的URL地址
	 * @param queryString
	 *            请求的查询参数,可以为null
	 * @return 返回请求响应的HTML
	 */
	private static String doGet(String url, String queryString) {
		String response = null;
		HttpClient client = new HttpClient();
		HttpMethod method = new GetMethod(url);
		try {
			if (StringUtils.isNotBlank(queryString))
				method.setQueryString(URIUtil.encodeQuery(queryString));
			client.executeMethod(method);
			if (method.getStatusCode() == HttpStatus.SC_OK) {
				response = method.getResponseBodyAsString();
			}
		} catch (URIException e) {
			log.error("com.mmb.sms.MMBSmsService.doGet(String, String),执行HTTP Get请求时，编码查询字符串“" + queryString + "”发生异常！", e);
		} catch (IOException e) {
			log.error("com.mmb.sms.MMBSmsService.doGet(String, String),执行HTTP Get请求" + url + "时，发生异常！", e);
		} finally {
			method.releaseConnection();
		}
		return response;
	}

	// http://121.40.60.163:8081/message/sendMsg?loginname=001&password=001&mobile=1368888888&content=短信&extNo=01

	private static final Map<String, String>	jianzhouMessage	= new HashMap<String, String>();
	static {

		jianzhouMessage.put("-1", "余额不足");
		jianzhouMessage.put("-2 ", "帐号或密码错误");
		jianzhouMessage.put("-3", "连接服务商失败");
		jianzhouMessage.put("-4", "超时");
		jianzhouMessage.put("-5", "其他错误，一般为网络问题，IP受限等");
		jianzhouMessage.put("-6", "短信内容为空");
		jianzhouMessage.put("-7", "目标号码为空");

		jianzhouMessage.put("-8", "用户通道设置不对，需要设置三个通道");
		jianzhouMessage.put("-9", "捕获未知异常");
		jianzhouMessage.put("-10", "超过最大定时时间限制");
		jianzhouMessage.put("-11", "目标号码在黑名单里");
		jianzhouMessage.put("-12", "消息内容包含禁用词语");
		jianzhouMessage.put("-13", "没有权限使用该网关");

		jianzhouMessage.put("-14", "找不到对应的Channel ID");
		jianzhouMessage.put("-17", "没有提交权限，客户端帐号无法使用接口提交");
		jianzhouMessage.put("-20", "超速提交(一般为每秒一次提交)");
	}

	
	/**
	 *  0	提交失败
		2	提交成功
		400	非法ip访问
		401	帐号不能为空
		402	密码不能为空
		403	手机号码不能为空
		4030	手机号码已被列入黑名单
		404	短信内容不能为空
		405	用户名或密码不正确
		4050	账号被冻结
		4051	剩余条数不足
		4052	访问ip与备案ip不符
		406	手机格式不正确
		407	短信内容含有敏感字符
		4070	签名格式不正确
		4071	没有提交备案模板
		4072	短信内容与模板不匹配
		4073	短信内容超出长度限制
		408	您的帐户疑被恶意利用，已被自动冻结，如有疑问请与客服联系。
	 */
	private static final Map<String, String>	huyiMessage	= new HashMap<String, String>();
	static {

		huyiMessage.put("0", "提交失败");
		huyiMessage.put("400 ", "非法ip访问");
		huyiMessage.put("401", "帐号不能为空");
		huyiMessage.put("402", "密码不能为空");
		huyiMessage.put("403", "手机号码不能为空");
		huyiMessage.put("4030", "手机号码已被列入黑名单");
		huyiMessage.put("404", "短信内容不能为空");

		huyiMessage.put("405", "用户名或密码不正确");
		huyiMessage.put("4050", "账号被冻结");
		huyiMessage.put("4051", "剩余条数不足");
		huyiMessage.put("4052", "访问ip与备案ip不符");
		huyiMessage.put("406", "手机格式不正确");
		huyiMessage.put("407", "短信内容含有敏感字符");

		huyiMessage.put("4070", "签名格式不正确");
		huyiMessage.put("4071", "没有提交备案模板");
		huyiMessage.put("4072", "短信内容与模板不匹配");
		huyiMessage.put("4073", "短信内容超出长度限制");
		huyiMessage.put("408", "您的帐户疑被恶意利用，已被自动冻结，如有疑问请与客服联系。");
	}

	
	/**
	 * 
	 * @author  liuze
	 * @date 2016年12月8日上午10:15:12
	 * @Description: 互亿无线短信发送
	 * @param
	 * http://106.ihuyi.com/webservice/sms.php?method=Submit&account=cf_xlct&password=XUj8PE&mobile=15800453479&content=您的验证码是：1234。请不要把验证码泄露给其他人。
	 */
	public static String sendSmsHuyi(String phone, String smsContent){
		log.info("互亿短信发送start");
		StringBuilder sb = new StringBuilder("method=Submit"+"&account=" + huyiUserName + "&password=" + huyiPassword + "&mobile=");
		sb.append(phone);
		sb.append("&content=");
		sb.append(smsContent);
		
		HttpClient client = new HttpClient();
		HttpMethod method = new GetMethod("http://106.ihuyi.com/webservice/sms.php");
		String response ="";
		try {
			if (StringUtils.isNotBlank(sb.toString()))
				method.setQueryString(URIUtil.encodeQuery(sb.toString()));
			client.executeMethod(method);
			if (method.getStatusCode() == HttpStatus.SC_OK) {
				response = method.getResponseBodyAsString();
				
				Map<String,Object> map = JaxbUtil.parse(response);
				if(!map.get("code").equals("2")){
					String msg  = huyiMessage.get(map.get("code").toString());
					String content = "互亿返回码："+map.get("code")+"，详细信息："+msg;
					log.info("互亿短信发送失败，原因："+content);
				}
			}
		} catch (IOException e) {
			log.error("com.mmb.sms.MMBSmsService.doGet(String, String),执行HTTP Get请求" + sb + "时，发生异常！", e);
		} finally {
			method.releaseConnection();
		}
		return response;

	}
}
