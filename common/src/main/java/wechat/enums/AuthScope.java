package wechat.enums;

public enum AuthScope {
    snsapiBase("snsapi_base","不弹出授权页面，直接跳转，只能获取用户openid"),
    snsapiUserinfo("snsapi_userinfo","弹出授权页面，可通过openid拿到昵称、性别、所在地。并且， 即使在未关注的情况下，只要用户授权，也能获取其信息");

    String code;
    String description;

    private AuthScope(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
