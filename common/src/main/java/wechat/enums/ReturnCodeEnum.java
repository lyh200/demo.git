package wechat.enums;

public enum ReturnCodeEnum {



    成功("SUCCESS","成功"),
    失败("FAIL","失败");

    String code;
    String description;

    private ReturnCodeEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
