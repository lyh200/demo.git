package demo;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Created by crj on 2018/2/2.
 */
public class NewFixedThread {

    private static AtomicInteger add = new AtomicInteger(0);
    //private static    int add =0;
    private static Object object = new Object();
    private  static  int number = 10000000;
    static CountDownLatch countDownLatch = new CountDownLatch(number);

    public static void main(String[] args) throws InterruptedException {
        ExecutorService service = Executors.newCachedThreadPool();
        long start = System.currentTimeMillis();
        for (int i = 0; i < number; i++) {
            service.execute(new DemoThread());
            System.out.println(add);
        }
        countDownLatch.await();
        System.out.println("result:" + add);
        long end = System.currentTimeMillis();
        System.out.println("耗时：" + (end - start));

    }


    static class DemoThread implements Runnable {
        @Override
        public void run() {
            add.incrementAndGet();
//            synchronized (object) {
//                add = add + 1;
//            }
            countDownLatch.countDown();

        }
    }
}
