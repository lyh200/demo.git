/**
 * 有N张火车票，每张票都有一个编号
 * 同时有10个窗口对外售票
 * 请写一个模拟程序
 * 
 * 分析下面的程序可能会产生哪些问题？
 *  
 * 使用Vector或者Collections.synchronizedXXX
 * 分析一下，这样能解决问题吗？
 * 
 * @author 马士兵
 */
package yxxy.c_024;

import java.util.Vector;
import java.util.concurrent.TimeUnit;

public class TicketSeller2 {
	static Vector<String> tickets = new Vector<>();//线程安全容器,size和remove都是原子操作函数
	static {
		for(int i=0; i<1000; i++) tickets.add("票 编号：" + i);
	}

	/**
	 * 虽然size和remove函数都是线程安全的原子操作函数，但是在这两个原子操作之间还是存在
	 * 被其他线程插入操作的可能性，即：判断与操作分离会导致线程不安全。
	 */
	public static void main(String[] args) {
		for(int i=0; i<10; i++) {
			new Thread(()->{
				while(tickets.size() > 0) {
					try {
						TimeUnit.MILLISECONDS.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println("销售了--" + tickets.remove(0));
				}
			}).start();
		}
	}
}
