package yxxy.c_025;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class T04_ConcurrentQueue {
	public static void main(String[] args) {
		//队列解耦合
		Queue<String> strs = new ConcurrentLinkedQueue<>();
		
		for(int i=0; i<10; i++) {
			strs.offer("a" + i);  //add
		}
		
		System.out.println(strs);
		
		System.out.println(strs.size());
		
		System.out.println(strs.poll());//取出并从队列中删掉
		System.out.println(strs.size());
		
		System.out.println(strs.peek());//只取出不从队列删掉
		System.out.println(strs.size());
		
		//双端队列Deque
	}
}
