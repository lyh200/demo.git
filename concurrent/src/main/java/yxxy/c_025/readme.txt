容器总结：
1：对于map/set的选择使用
//非并发
HashMap
TreeMap
LinkedHashMap
//并发量比较小
Hashtable
Collections.sychronizedXXX
//并发量比较大
ConcurrentHashMap
ConcurrentSkipListMap//类似TreeMap,支持排序

2：队列
//非线程安全
ArrayList
LinkedList
//并发量比较小
vector
Collections.synchronizedXXX
//并发量比较大
Queue
	CocurrentLinkedQueue //非阻塞并发队列：加锁机制实现,注意无concurrentArrayQueue
	BlockingQueue//阻塞队列：不加锁
		LinkedBQ//无界队列
		ArrayBQ //有界队列
		TransferQueue
		SynchronusQueue
	DelayQueue//执行定时任务
	CopyOnWriteList//读的线程多,写的线程少,读时不需要加锁
		
	