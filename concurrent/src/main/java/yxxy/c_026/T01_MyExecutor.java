/**
 * 认识Executor
 */
package yxxy.c_026;
import java.util.concurrent.Executor;

/**
 * Executor是一个接口，只有一个方法void execute(Runnable command);
 *
 */
public class T01_MyExecutor implements Executor {

	public static void main(String[] args) {
		new T01_MyExecutor().execute(
				()->System.out.println("hello executor"));
	}

	@Override
	public void execute(Runnable command) {
		//new Thread(command).start();//新起一个线程去执行Runnable
		command.run();//Runnable的run方法调用，并未新建一个线程
	}

}

