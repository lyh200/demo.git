/**
 * 认识future
 */
package yxxy.c_026;

import java.util.concurrent.*;

public class T06_Future {
	public static void main(String[] args)
			throws InterruptedException, ExecutionException {
		/**
		 * 虽然Callable表示一个有返回值的线程任务，但Callable接口中只有一个方法
		 * V call() throws Exception;并没有获取返回值的方法，
		 * 因此需要用FutureTask包装Callable,利用FutureTask的阻塞方法get来获取
		 * Callable的返回值。
		 */
		FutureTask<Integer> task = new FutureTask<>(()->{
			TimeUnit.MILLISECONDS.sleep(500);
			return 1000;
		}); //new Callable () { Integer call();}

		new Thread(task).start();
		System.out.println(task.get()); //阻塞
		
		//*******************************
		ExecutorService service = Executors.newFixedThreadPool(5);
		Future<Integer> f = service.submit(()->{
			TimeUnit.MILLISECONDS.sleep(500);
			return 1;
		});
		System.out.println(f.get());
		System.out.println(f.isDone());
	}
}
