package cn.lyh.sharding.service;



import cn.lyh.sharding.domain.Parents;

import java.util.List;

/**
 * @author Ken
 * @Description: TODO
 * @date 2021/3/11 20:55
 */
public interface IParentsService {

    /**
     * 保存家长信息
     *
     * @param suiteId
     * @param parents
     * @throws Exception
     */
    void saveParents(String suiteId, Parents parents) throws Exception;



    /**
     * 获取家长身份信息
     *
     * @param unionId
     * @return
     */
    List<Parents> getParents(String unionId, String schoolId, Long classId);

    /**
     * 获取家长身份信息
     *
     * @param schoolId
     * @return
     */
    List<Parents> getSchoolParents(String schoolId);

    List<Parents> listParentsByStudent(String schoolId, String wxStudentId);

    void deleteByIds(String wxSchoolId, List<Long> ids);

    void deleteByWxClassIds(String wxSchoolId, List<Long> wxClassIds);


}
