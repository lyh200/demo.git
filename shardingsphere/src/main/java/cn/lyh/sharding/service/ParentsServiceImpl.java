package cn.lyh.sharding.service;


import cn.lyh.sharding.dao.ParentsMapper;
import cn.lyh.sharding.domain.Parents;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author Ken
 * @Description: TODO
 * @date 2021/3/11 20:59
 */
@Slf4j
@Service
public class ParentsServiceImpl implements IParentsService {

    @Resource
    private ParentsMapper parentsMapper;

    @Override
    public void saveParents(String suiteId, Parents parents) {

        String wxCorpId = parents.getWxSchoolId();
        String studentId = parents.getWxStudentId();
        String wxParentId = parents.getWxParentId();
        Long wxClassId = parents.getWxClassId();

                    parentsMapper.insert(parents);
                    // 保存家长信息
                    log.info("SAVE_PARENTS_INSERT school:【{}】,班级:【{}】,家长：【{}】"
                            , wxCorpId, parents.getWxClassId(), wxParentId);



    }

    private void deleteParentsInfo(String wxCorpId, String wxParentId) {
        LambdaQueryWrapper<Parents> wrapper = new QueryWrapper<Parents>().lambda()
                .eq(Parents::getWxSchoolId, wxCorpId)
                .eq(Parents::getWxParentId, wxParentId);


        Parents parents = new Parents();
        parentsMapper.update(parents, wrapper);
        log.info("DELETE_PARENTS:wxCorpId：【{}】- wxParentId【{}】", wxCorpId, wxParentId);
    }


    @Override
    public List<Parents> getParents(String unionId, String schoolId, Long classId) {
        LambdaQueryWrapper<Parents> wrapper = new LambdaQueryWrapper<Parents>();
            wrapper.eq(Parents::getWxSchoolId, schoolId);
            if (null != classId && classId > 0) {

                wrapper.eq(Parents::getWxClassId, classId);
            }


        List<Parents> parents = parentsMapper.selectList(wrapper);
        return parents;
    }

    @Override
    public List<Parents> getSchoolParents(String schoolId) {
        LambdaQueryWrapper<Parents> wrapper = new QueryWrapper<Parents>().lambda()
                .eq(Parents::getWxSchoolId, schoolId);

        List<Parents> parents = parentsMapper.selectList(wrapper);
        return parents;
    }

    @Override
    public List<Parents> listParentsByStudent(String schoolId, String wxStudentId) {
        LambdaQueryWrapper<Parents> wrapper = new QueryWrapper<Parents>().lambda()
                .eq(Parents::getWxSchoolId, schoolId)
                .eq(Parents::getWxStudentId, wxStudentId);

        List<Parents> parents = parentsMapper.selectList(wrapper);
        return parents;
    }

    @Override
    public void deleteByIds(String wxSchoolId, List<Long> ids) {


        LambdaQueryWrapper<Parents> wrapper = new QueryWrapper<Parents>().lambda()
                .eq(Parents::getWxSchoolId, wxSchoolId)
                .in(Parents::getId, ids);

        Parents student = new Parents();
        parentsMapper.update(student, wrapper);
        log.info("PARENTS-DELETE-IDS,wxSchoolId:{}- ids:{}", wxSchoolId, ids);
    }


    @Override
    public void deleteByWxClassIds(String wxSchoolId, List<Long> wxClassIds) {

        LambdaQueryWrapper<Parents> wrapper = new QueryWrapper<Parents>().lambda()
                .eq(Parents::getWxSchoolId, wxSchoolId)
                .in(Parents::getWxClassId, wxClassIds);

        Parents student = new Parents();
        parentsMapper.update(student, wrapper);
        log.info("PARENTS-DELETE-WXCLASSIDS,wxSchoolId:{} wxClassIds:{}", wxSchoolId, wxClassIds);
    }


}
