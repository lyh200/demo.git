package cn.lyh.sharding;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("cn.lyh.sharding.dao")
public class SharingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SharingApplication.class,args );
    }
}
