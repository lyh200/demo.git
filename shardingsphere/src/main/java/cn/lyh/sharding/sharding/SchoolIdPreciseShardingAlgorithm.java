package cn.lyh.sharding.sharding;

import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.util.Collection;

public class SchoolIdPreciseShardingAlgorithm  implements PreciseShardingAlgorithm {
    @Override
    public String doSharding(Collection collection, PreciseShardingValue preciseShardingValue) {
     String wxSchoolId = (String) preciseShardingValue.getValue();

        int  x =wxSchoolId.hashCode();
        int  table =Math.abs(x)%4;
        String tableReal = preciseShardingValue.getLogicTableName().concat("_").concat(table+"");
        return tableReal;
    }
}
