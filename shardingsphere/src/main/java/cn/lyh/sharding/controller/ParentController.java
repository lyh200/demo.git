package cn.lyh.sharding.controller;

import cn.lyh.sharding.domain.Parents;
import cn.lyh.sharding.service.IParentsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/test")
public class ParentController {

    @Resource
    private IParentsService parentsService;



    @GetMapping("/query")
    public Object info(
                       @RequestParam(value = "school_id", required = false) String schoolId,
                       @RequestParam(value = "class_id") Long classId){

        List<Parents> parents = parentsService.getParents(null,schoolId, classId);
        return parents;
    }
}
