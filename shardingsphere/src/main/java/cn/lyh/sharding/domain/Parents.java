package cn.lyh.sharding.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author shans
 * @since 2022-07-29
 */
@Getter
@Setter
@TableName("t_parents")
@ApiModel(value = "Parents对象", description = "")
public class Parents implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;



    @ApiModelProperty("微信的学校ID（企业ID）")
    @TableField("wx_school_id")
    private String wxSchoolId;

    @ApiModelProperty("孩子的家校通讯录ID")
    @TableField("wx_student_id")
    private String wxStudentId;




    @ApiModelProperty("家长的家校通讯录ID")
    @TableField("wx_parent_id")
    private String wxParentId;





    @TableField("wx_class_id")
    private Long wxClassId;




}