package cn.lyh.mySpringTest.domain;

/**
 * Created by eason on 2017/4/26.
 */
public class User {

    private long id;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
