package cn.lyh.mySpringTest.controller;

import cn.lyh.mySpring.annotation.*;
import cn.lyh.mySpringTest.domain.User;
import cn.lyh.mySpringTest.service.TestService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;


@MyController
@MyRequestMapping("/test")
public class TestController {
    @MyAutowired
    private TestService testService;

    @MyRequestMapping("test1")
    public String test1(@MyRequestParam("name") String name,
                        @MyRequestParam("sex") Integer sex,
                        HttpServletRequest request,
                        HttpServletResponse response) throws IOException {

        return "name=" + name + "sex=" + sex;
    }


    @MyRequestMapping("test2")
    public void test2() {


    }


    @MyRequestMapping("test3")
    @MyResponseBody
    public Map<String, Object> test3(@MyRequestParam("name") String name,
                                     @MyRequestParam("sex") Integer sex,
                                     HttpServletRequest request,
                                     HttpServletResponse response) throws IOException {
        Map<String, Object> result = new HashMap<>();
        result.put("name", name);
        result.put("sex", name);

        return result;
    }

    @MyRequestMapping("test4")
    @MyResponseBody
    public User test4(@MyRequestParam("name") String name,
                      @MyRequestParam("sex") Integer sex,
                      HttpServletRequest request,
                      HttpServletResponse response) throws IOException {
        User user = new User();
        user.setName(name);
        user.setId(sex);

        return user;
    }

    @MyRequestMapping("test5")
    @MyResponseBody
    public List test5(@MyRequestParam("name") String name,
                      @MyRequestParam("sex") Integer sex,
                      HttpServletRequest request,
                      HttpServletResponse response) throws IOException {
        List list = new ArrayList();
        User user = new User();
        user.setName(name);
        user.setId(sex);
        list.add(user);

        return list;
    }

    @MyRequestMapping("test6")
    @MyResponseBody
    public List test5(HttpServletRequest request,
                      HttpServletResponse response) throws IOException {
        List list = new ArrayList();
        User user = new User();
        user.setName(null);
        user.setId(1);
        list.add(user);

        return list;
    }


}
