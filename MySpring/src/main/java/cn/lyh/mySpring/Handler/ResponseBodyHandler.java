package cn.lyh.mySpring.Handler;


public interface ResponseBodyHandler {

    Object excute(Object body, Class returnType);
}
