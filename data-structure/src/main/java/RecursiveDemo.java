/**
 * Created by eason on 2017/12/16.
 * @author liuynghui
 */
public class RecursiveDemo
{
    public static void main(String[] args) {
        System.out.println( demo(100));

    }

    /**
     * 递归demo 计算 1+2+3+...+n
     *
     */
    public static int  demo(int i) {
        if(i==1){
            return 1;
        }else{
            return demo(i-1)+i;
        }
    }

}
