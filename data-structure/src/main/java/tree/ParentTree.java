package tree;




/**
 * Created by crj on 2017/12/4.
 * 树的实现
 * 基于双亲节点表示法
 */
public class ParentTree<T> {

    private final int DEFAULT_TREE_SIZE = 100;
    private int treeSize = 0;
      // 使用一个Node[]数组来记录该树里的所有节点
    private Node<T>[] nodes;
    // 记录树的节点数
    private int nodeNums;

    ParentTree(){
        nodes = new Node[DEFAULT_TREE_SIZE];
        treeSize =0;

    }
    class Node<E>{
        E data;
        int  parentNode;

        public E getData() {
            return data;
        }

        public void setData(E data) {
            this.data = data;
        }

        public int getParentNode() {
            return parentNode;
        }

        public void setParentNode(int parentNode) {
            this.parentNode = parentNode;
        }
    }
}
