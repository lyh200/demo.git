package queue;

/****
 * 队列ADT
 * @param <E>
 */
public interface Queue<E> {


    /***
     * 清空队列
     */
    void clear();

    /***
     * 队列是否为空
     * @return
     */

    boolean isEmpty();

    /***
     * 获取头
     * @return
     */
    E getHead();

    /***
     * 队尾添加元素
     */
    void add(E e);

    /***
     * 头元素出
     */
    void remove();

    /***
     * 返回队列长度
     * @return
     */
    int size();
}
