package linearList;

/**
 * Created by eason on 2017/11/18.
 */
public interface MyList<E> {

    Integer size();

    Object get(int i);

    boolean add(E  object);

    boolean remove(int i);
    boolean empty();


}
