//import linearList.LinkedList;

import linearList.MySiglelyLinkedList;
import queue.Queue;
import queue.RoundRobinQueue;
import tree.BinaryTree;

import java.util.LinkedList;

//import java.util.LinkedList;


/**
 * Created by eason on 2017/11/18.
 */
public class test {
    public static void main(String[] args) {
//        ArrayList list  =new ArrayList(5);
//        for(int i=0;i<10000
//                ;i++) {
//            list.add(i);
//        }
//       list.add(5,"插入1");
//        list.add(5,"插入2");
//        list.add(5,"插入3");
//        for(int i =0;i<list.size();i++){
//            System.out.println(list.get(i));
//        }
//        LinkedList lList = new LinkedList();
//        lList.add("1");
//        lList.add("2");
//        lList.add("3");
//        lList.add("4");
//        lList.add("5");
//        lList.add("6");
//       System.out.print( lList.get(1));

//       LinkedList List1 =new LinkedList();
//        List1.add("1");
//        List1.add("2");
//        List1.add("3");
//        List1.add("4");
//        List1.add("5");
//        List1.add("6");
//        System.out.print( List1.get(1));
//        MySiglelyLinkedList mySiglelyLinkedList  =new MySiglelyLinkedList();
//        mySiglelyLinkedList.add("1");
//        mySiglelyLinkedList.add("2");
//        mySiglelyLinkedList.add("3");
//        mySiglelyLinkedList.add("4");
//        mySiglelyLinkedList.add("5");
//        mySiglelyLinkedList.add("1.5",1);
//        mySiglelyLinkedList.add("6");
//        System.out.println(mySiglelyLinkedList.display());
/***
 *                          a
 *             b                         c
 *        d         e             f
 *               i     j             g
 *                                      h
 *
 */
//        BinaryTree<String> binaryTree = new BinaryTree<>();
//        BinaryTree.Node<String> i = new BinaryTree.Node("i", null, null);
//        BinaryTree.Node<String> j = new BinaryTree.Node("j", null, null);
//        BinaryTree.Node<String> h = new BinaryTree.Node("h", null, null);
//        BinaryTree.Node<String> g = new BinaryTree.Node("g", null, h);
//        BinaryTree.Node<String> e = new BinaryTree.Node("e", i, j);
//        BinaryTree.Node<String> d = new BinaryTree.Node("d", null, null);
//        BinaryTree.Node<String> f = new BinaryTree.Node("f", null, g);
//
//        BinaryTree.Node<String> b = new BinaryTree.Node("b", d, e);
//        BinaryTree.Node<String> c = new BinaryTree.Node("c", f, null);
//        BinaryTree.Node<String> root = new BinaryTree.Node("a", b, c);
//
//        binaryTree.setRoot(root);
//        System.out.println("前序遍历》》》》》》》》》》》》》");
//        binaryTree.preOrder(binaryTree.getRoot());
//        System.out.println("");
//        System.out.println("非递归--前序遍历》》》》》》》》》》》》》");
//        binaryTree.nonRecPreOrder(binaryTree.getRoot());
//        System.out.println();
//        System.out.println("中序》》》》》》》》》》》》》");
//        binaryTree.inOrder(binaryTree.getRoot());
//        System.out.println("");
//        System.out.println("非递归--中续遍历》》》》》》》》》》》》》");
//        binaryTree.nonRecInOrder(binaryTree.getRoot());
//        System.out.println();
//        System.out.println("后续》》》》》》》》》》》》》");
//        binaryTree.postOrder(binaryTree.getRoot());
//        System.out.println();
//        System.out.println("后续非递归》》》》》》》》》》》》》");
//        binaryTree.nonRecPostOrder(binaryTree.getRoot());
//
//        System.out.println();
//        System.out.println("层次遍历》》》》》》》》》》》》》");
//        binaryTree.layerOrder(binaryTree.getRoot());

        RoundRobinQueue();

    }

    public static void RoundRobinQueue() {
        Queue<Integer> queue = new RoundRobinQueue<>(10);
        System.out.println("队列长度：" + queue.size());
        queue.add(0);
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
        queue.add(5);
        queue.add(6);
        queue.add(7);
        queue.add(8);
        queue.add(9);
        queue.remove();
        queue.add(9);
        System.out.println("队列长度：" + queue.size() + " " + queue.isEmpty());
        queue.clear();
        queue.add(10);
        queue.add(11);
        queue.add(12);


        System.out.println("--- 队列长度：" + queue.size());
        queue.remove();

    }


}
