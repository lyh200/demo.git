import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.*;

/**
 * Created by crj on 2017/11/14.
 */
public class SqlSessionFactoryBuilderTest {


    public static void main(String[] args) throws FileNotFoundException {
        String resource = "config.xml";
        Reader reader;
        try {
            reader = Resources.getResourceAsReader(resource);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        SqlSessionFactoryBuilder sfb  = new SqlSessionFactoryBuilder();
        SqlSessionFactory sqlSessionFactory =sfb.build(reader);
        SqlSession sqlSession =sqlSessionFactory.openSession();

    }

}
