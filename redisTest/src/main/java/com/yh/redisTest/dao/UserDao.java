package com.yh.redisTest.dao;

import com.yh.redisTest.domain.User;

/**
 * Created by eason on 2017/4/26.
 */
public interface UserDao {

    public void saveUser(final User user);
    public User getUser(final long id);

    public void testLock();
}
