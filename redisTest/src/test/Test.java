import com.yh.redisTest.dao.UserDao;
import com.yh.redisTest.domain.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by eason on 2017/4/26.
 *
 * 测试分布式锁
 *众多线程只能执行一次任务只能执行一次
 */
public class Test {
    public static void main(String[] args) {
        ApplicationContext ac =  new ClassPathXmlApplicationContext("classpath:/applicationContext.xml");
        UserDao userDAO = (UserDao)ac.getBean("userDAO");
//        User user1 = new User();
//        user1.setId(2);
//        user1.setName("obama2");
//        userDAO.saveUser(user1);
//        User user2 = userDAO.getUser(1);
//        System.out.println(user2.getName());
        ExecutorService executorService  = Executors.newFixedThreadPool(1000);
        for(int i=0;i<10;i++) {
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    userDAO.testLock();
                }
            });
        }

        //executorService.shutdown();
    }
}
