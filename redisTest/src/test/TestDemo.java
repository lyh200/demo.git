import com.yh.redisTest.dao.UserDao;
import com.yh.redisTest.domain.User;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by crj on 2017/11/16.
 */
@RunWith(value= SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:applicationContext.xml")
public class TestDemo
{
    @Autowired
    UserDao  userDao;
    @org.junit.Test
    public  void test(){
//        User user1 = new User();
//        user1.setId(2);
//        user1.setName("obama2");
//        userDao.saveUser(user1);
//        User user2 = userDao.getUser(2);
//        System.out.println(user2.getName());
        ExecutorService executorService  = Executors.newFixedThreadPool(1000);
        try {
            for (int i = 0; i < 10000; i++) {
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        userDao.testLock();
                    }
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        finally{
            executorService.shutdown();
        }
    }
}
