package com.yh.factoryMethod;

/**
 * Created by liuyonghui on 17/3/29.
 * 工厂类
 */
public interface Factory {

    public double operation(double number1, double number2);
}
