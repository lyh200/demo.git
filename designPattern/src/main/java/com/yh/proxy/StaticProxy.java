package com.yh.proxy;

import com.yh.proxy.target.Task;

/**
 * Created by eason on 2017/10/10.
 */
public class StaticProxy implements Task {
    private Task task;
    public StaticProxy(Task task){
        this.task =task;
    }
    @Override
    public void add() {
        task.add();
    }

    @Override
    public void delete() {
     task.delete();
    }
}
