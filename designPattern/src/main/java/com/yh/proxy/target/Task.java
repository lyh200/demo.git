package com.yh.proxy.target;

/**
 * Created by eason on 2017/10/10.
 */
public interface Task {
    void add();
    void delete();
}
