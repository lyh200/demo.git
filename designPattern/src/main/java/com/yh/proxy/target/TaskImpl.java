package com.yh.proxy.target;

/**
 * Created by eason on 2017/10/10.
 */
public class TaskImpl implements  Task {
    @Override
    public void add() {
        System.out.println("add");
    }

    @Override
    public void delete() {
        System.out.println("delete");
    }
}
