package com.yh.proxy.task;

import com.yh.proxy.CglibProxy;
import com.yh.proxy.DynamicProxy;
import com.yh.proxy.JDKDynamicProxy;
import com.yh.proxy.StaticProxy;
import com.yh.proxy.target.Hello;
import com.yh.proxy.target.Task;
import com.yh.proxy.target.TaskImpl;
import net.sf.cglib.core.DebuggingClassWriter;

/**
 * Created by eason on 2017/10/10.
 *
 * jdk中的动态代理
 * 通过反射类Proxy和InvocationHandler回调接口实现，要求委托类必须实现一个接口，
 * 只能对该类接口中定义的方法实现代理，这在实际编程中有一定的局限性
 * 使用cglibd代理
 * 并不要求委托类必须实现接口，底层采用asm字节码生成框架生成代理类的字节码，不能处理被final关键字修饰的方法
 *
 */
public class Client {

    /**
     * @param args
     */
    public static void main(String[] args) throws IllegalAccessException, InstantiationException {

        //动态代理
        System.out.println("动态代理————————————————————");
        DynamicProxy logHandler=new DynamicProxy();
       // Task userManager=(Task)logHandler.newProxyInstance(new TaskImpl());
       // userManager.add();

        //静态代理
        //正常思路：客户端直接实例化出 子类的对象
        System.out.println("静态代理————————————————————");
        TaskImpl task=new TaskImpl();
        //静态代理：客户端实例化代理，通过代理取 子类的引用
        StaticProxy staticProxy = new StaticProxy(task);
        staticProxy.add();


        //Cglib代理  启动参数-Dcglib.debugLocation="具体路径"
        System.out.println("cglib动态代理————————————————————");
        System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY,"/Users/cherry/IdeaProjects/demo/designPattern/target/classes/com/yh/proxy/target");

        CglibProxy cglibProxy = new CglibProxy();
        Task task1 = (Task) cglibProxy.getProxy(TaskImpl.class);
        task1.add();
        CglibProxy cglibProxy2 = new CglibProxy();
        Hello hello = (Hello) cglibProxy2.getProxy(Hello.class);
        hello.say();

        //jdk代理
        System.out.println("jdk动态代理————————————————————");
        JDKDynamicProxy jdkDynamicProxy = new JDKDynamicProxy(TaskImpl.class.newInstance());
        Task task2 =jdkDynamicProxy.getProxy();
        task2.add();

        System.out.println(3&5);

        System.out.println(3&(5<<2));
    }
}
