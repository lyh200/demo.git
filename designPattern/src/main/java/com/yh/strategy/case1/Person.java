package com.yh.strategy.case1;

/**
 * Created by eason on 2017/12/19.
 */
public class Person {

    private  VehicleStrategy vehicleStrategy;
    private  String  name;

    public void handleByVehicle(){
        this.vehicleStrategy.vehicel();

    }
    Person(VehicleStrategy vehicleStrategy){
        this.vehicleStrategy =vehicleStrategy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
