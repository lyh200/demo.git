package com.yh.strategy.case1;

/**
 * Created by eason on 2017/12/19.
 * @author  lyh
 */
public interface VehicleStrategy {
    /***
     * 交通工具
     */
    void  vehicel();
}
