package com.yh.strategy.case1;

/**
 * Created by eason on 2017/12/19.
 * @author lyh
 */
public class Subway  implements VehicleStrategy {
    @Override
    public void vehicel() {
        System.out.println("乘坐地铁上班");
    }
}
