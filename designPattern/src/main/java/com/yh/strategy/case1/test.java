package com.yh.strategy.case1;

/**
 * Created by eason on 2017/12/19.
 * @author  lyh
 */
public class test {

    public static void main(String[] args) {
        //策略
        Ride ride = new Ride();
        Subway subway = new Subway();

        Person  person = new Person(ride);
        person.setName("小明");
        System.out.println(person.getName());
        person.handleByVehicle();


        Person  person1 = new Person(subway);
        person1.setName("小明");
        System.out.println(person1.getName());
        person1.handleByVehicle();
    }
}
