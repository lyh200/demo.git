package com.yh.strategy.case2.handler;

import com.yh.strategy.case2.Order;
import com.yh.strategy.case2.OrderHandlerType;

@OrderHandlerType(source = "mobile")
public class MobileOrderHandler implements OrderHandler {
    @Override
    public void handle(Order order) {
        System.out.println("处理移动端订单");
    }
}