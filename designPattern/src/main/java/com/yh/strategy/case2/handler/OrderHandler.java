package com.yh.strategy.case2.handler;

import com.yh.strategy.case2.Order;

public interface OrderHandler {
    void handle(Order order);

}
