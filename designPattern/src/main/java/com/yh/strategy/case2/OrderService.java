package com.yh.strategy.case2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Service;
import com.yh.strategy.case2.handler.OrderHandler;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class OrderService {

    private Map<String, OrderHandler> orderHandleMap;

    /****
     * 自动注入 策略
     * @param orderHandlers
     */
    @Autowired
    public void setOrderHandleMap(List<OrderHandler> orderHandlers) {
        // 注入各种类型的订单处理类
        orderHandleMap = orderHandlers.stream().collect(
                Collectors.toMap(orderHandler -> AnnotationUtils.findAnnotation(orderHandler.getClass(), OrderHandlerType.class).source(),
                        v -> v, (v1, v2) -> v1));
    }

    public void orderService(Order order) {
        OrderHandler orderHandler = orderHandleMap.get(order.getSource());
        orderHandler.handle(order);

    }
}