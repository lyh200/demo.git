package com.yh.simpleFactory;

/**
 * Created by liuyonghui on 17/3/17.
 * 抽象
 */
public abstract class Operation {
    Double numberA;
    Double numberB;

    abstract double getResult();

    public Double getNumberA() {
        return numberA;
    }

    public void setNumberA(Double numberA) {
        this.numberA = numberA;
    }

    public Double getGetNumberb() {
        return numberB;
    }

    public void setGetNumberb(Double getNumberb) {
        this.numberB = getNumberb;
    }


}
