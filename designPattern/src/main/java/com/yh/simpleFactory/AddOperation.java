package com.yh.simpleFactory;

/**
 * Created by liuyonghui on 17/3/17.
 */
public class AddOperation extends Operation {

    @Override
    double getResult() {
        return numberA+numberB;
    }
}
