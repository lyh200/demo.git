package com.yh.simpleFactory;

import com.yh.simpleFactory.factory.OperationFactory;

/**
 * Created by liuyonghui on 17/3/17.
 */
public class test {
    public static  void main(String[] args){
        Operation operation =OperationFactory.createOperation("+");
        operation.setNumberA(1D);
        operation.setGetNumberb(2D);
        System.out.printf(""+operation.getResult());
    }
}
