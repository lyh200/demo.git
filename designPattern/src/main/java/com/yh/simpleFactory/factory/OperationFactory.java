package com.yh.simpleFactory.factory;

import com.yh.simpleFactory.AddOperation;

import com.yh.simpleFactory.Operation;
import com.yh.simpleFactory.subOperation;

/**
 * Created by liuyonghui on 17/3/17.
 * 工厂
 */
public class OperationFactory {
    public static Operation createOperation(String operate){
        Operation operation =null;
        switch (operate){
            case "+": operation =new AddOperation(); break;
            case "-": operation = new subOperation(); break;
        }

        return operation;

    }
}
