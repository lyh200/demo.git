package com.yh.controller;

import com.yh.strategy.case2.Order;
import com.yh.strategy.case2.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseBody
class HelloController {

    @Autowired
    OrderService orderService;

    @PostMapping("/case")
    public String hello(@RequestBody Order order){
        orderService.orderService(order);
        return "你好啊:" + order.getSource();
    }
}