package com.yh.visitor;

import com.yh.visitor.client.ComputerPartDisplayVisitor;

/**
 * Created by liuyonghui on 17/3/29.
 */
public class VisitorPatternDemo {
    public static void main(String[] args) {

        ComputerPart computer = new Computer();
        computer.accept(new ComputerPartDisplayVisitor());
    }
}
