package com.yh.visitor.client;

import com.yh.visitor.Computer;
import com.yh.visitor.Keyboard;
import com.yh.visitor.Monitor;
import com.yh.visitor.Mouse;

/**
 * Created by liuyonghui on 17/3/29.
 */
public class ComputerPartDisplayVisitor implements ComputerPartVisitor {
    @Override
    public void visit(Computer computer) {
        System.out.println("Displaying Computer.");
    }

    @Override
    public void visit(Mouse mouse) {
        System.out.println("Displaying Mouse.");
    }

    @Override
    public void visit(Keyboard keyboard) {
        System.out.println("Displaying Keyboard.");
    }

    @Override
    public void visit(Monitor monitor) {
        System.out.println("Displaying Monitor.");
    }
}
