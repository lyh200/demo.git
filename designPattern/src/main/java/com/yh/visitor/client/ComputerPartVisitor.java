package com.yh.visitor.client;

import com.yh.visitor.Computer;
import com.yh.visitor.Keyboard;
import com.yh.visitor.Monitor;
import com.yh.visitor.Mouse;

/**
 * Created by liuyonghui on 17/3/29.
 */
public interface ComputerPartVisitor {

    public void visit(Computer computer);
    public void visit(Mouse mouse);
    public void visit(Keyboard keyboard);
    public void visit(Monitor monitor);
}
