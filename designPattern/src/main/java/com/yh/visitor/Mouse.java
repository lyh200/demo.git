package com.yh.visitor;

import com.yh.visitor.client.ComputerPartVisitor;

/**
 * Created by liuyonghui on 17/3/29.
 */
public class Mouse implements  ComputerPart {
    @Override
    public void accept(ComputerPartVisitor computerPartVisitor) {
        computerPartVisitor.visit(this);
    }
}
