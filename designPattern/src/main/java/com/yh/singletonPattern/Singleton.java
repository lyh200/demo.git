package com.yh.singletonPattern;

/**
 * Created by crj on 2017/9/6.
 */

/***
 * 匿名内部类方式实现
 * 利用了classloder的机制来保证初始化instance时只有一个线程
 */
public class Singleton {
    private static class SingletonHolder {
        private static final Singleton INSTANCE = new Singleton();
    }
    private Singleton (){}
    public static final Singleton getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
