package reflect;

/****
 *
 */
public class ReflectDemo {
    public String name;
    public int sex;

    ReflectDemo(){};

    public ReflectDemo(String name, int sex) {
        this.name = name;
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "ReflectDemo{" +
            "name='" + name + '\'' +
            ", sex=" + sex +
            '}';
    }

    private String say() {
        return "ReflectDemo{" +
            "name='" + name + '\'' +
            ", sex=" + sex +
            '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }
}
