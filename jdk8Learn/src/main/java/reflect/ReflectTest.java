package reflect;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectTest {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
       /****字节码获取方式***/
        //forName 形式来 获取到类
        Class forNameClass  =Class.forName("reflect.ReflectDemo");
        //通过 类名.class
        Class  reflectDemoClass=ReflectDemo.class;
        //通过实例.getClass
        ReflectDemo instance = new ReflectDemo();
        Class instanceClass  = instance.getClass();

        ReflectDemo reflectDemo = (ReflectDemo) getInstance(reflectDemoClass);


        Method method =forNameClass.getMethod("setName",String.class);
        method.invoke(reflectDemo,"123");
        System.out.println(reflectDemo.toString());
        //私有调用
        Method say =forNameClass.getDeclaredMethod("say");
        say.setAccessible(true);
        System.out.println(say.invoke(reflectDemo));



    }
    public static  Object getInstance(Class clazz) throws IllegalAccessException, InstantiationException {

        return clazz.newInstance();
    }
}
