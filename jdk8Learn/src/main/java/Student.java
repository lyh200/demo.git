import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by eason on 2017/7/25.
 */
@Data
@AllArgsConstructor
public class Student {
    private Integer no;
    private String name;
    private String age;
    private Integer sex;



}
