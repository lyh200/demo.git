package ClassLoder;

import java.util.LinkedHashMap;

/****
 *
 */
public class ClassLoad {
    public static void main(String[] args) {
        System.out.println("Bootstrap ClassLoader path: ");
        System.out.println(System.getProperty("sun.boot.class.path"));
        System.out.println("----------------------------");

        System.out.println("Extension ClassLoader path: ");
        System.out.println(System.getProperty("java.ext.dirs"));
        System.out.println("----------------------------");

        System.out.println("App ClassLoader path: ");
        System.out.println(System.getProperty("java.class.path"));
        System.out.println("----------------------------");
        LinkedHashMap lmp = new LinkedHashMap();
        lmp.put("1","2");
        lmp.put("2","3");
        lmp.put("4","5");
        lmp.put("6","7");

        lmp.values();

    }
}
