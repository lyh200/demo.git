/***
 * JAVA8-让代码更优雅之List排序
 */

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by crj on 2017/7/25.
 */
public class ListStream {
    public static void main(String[] args) {
        List<Student> list =new ArrayList<>();
        list.add( new Student(1,"小明","9",1));
        list.add( new Student(2,"小红","9",2));
        list.add( new Student(3,"小婷","10",1));
        list.add( new Student(4,"鸣人","8",2));
        list.add( new Student(5,"瑛","10",1));
        list.add( new Student(6,"婷婷","9",1));
        list.add( new Student(7,"潇潇","8",1));
        list.add( new Student(8,"莉莉","10",1));
       List<Demo> demos = new ArrayList<Demo>();
        //原始数据
        System.out.println("原始数据 组装list<demo>*******************");
        demos = list.stream().map(student-> new Demo(student.getAge(),student.getSex())).collect(Collectors.toList());
        demos.forEach(demo -> {
            System.out.print("年龄 "+demo.getAge() +"  性别 " +demo.getSex()+",");
        });
        System.out.println();
        System.out.println("原始数据 倒序list<demo>*******************");
        demos.sort(Comparator.comparing(Demo::getSex).reversed());
        demos.forEach(demo -> {
            System.out.print("年龄 "+demo.getAge() +"  性别 " +demo.getSex()+"     ");
        });
        System.out.println();
        //只取sex为0
        System.out.println("只取sex为0****************");
        List<Demo> demorm =demos.stream().filter(demo -> demo.getSex() == 0).distinct().collect(Collectors.toList());
        demorm.forEach(demo -> {
            System.out.print("年龄 "+demo.getAge() +"  性别 " +demo.getSex()+"     ");
        });
        System.out.println();

        //筛选年龄大于12岁
        System.out.println("筛选年龄大于12岁的*************");
        List<Demo> demoFilter =  demos.stream().filter(demo -> Integer.valueOf(demo.getAge()) > 12).collect(Collectors.toList());
        demoFilter.forEach(demo -> {
            System.out.print("年龄 "+demo.getAge() +"  性别 " +demo.getSex()+"     ");
        });
        System.out.println();
        //排序
        System.out.println("排序******************");
        List<Demo> demoSort = demos.stream().sorted((s1, s2) -> s1.getAge().compareTo(s2.getAge())).collect(Collectors.toList());
        demoSort.forEach(demo -> {
            System.out.print("年龄 "+demo.getAge() +"  性别 " +demo.getSex()+"     ");
        });
        System.out.println();
        //倒序
        System.out.println("倒序****************");
        ArrayList<Demo> demoArray = (ArrayList<Demo>) demos;
        Comparator<Demo> comparator = (h1, h2) -> h1.getAge().compareTo(h2.getAge());
        demos.sort(comparator.reversed());
        demos.forEach(demo -> {
            System.out.print("年龄 "+demo.getAge() +"  性别 " +demo.getSex()+"     ");
        });
        System.out.println();

        //多条件正序
        System.out.println("多条件排序正序****************");
        demoArray.sort(Comparator.comparing(Demo::getSex).thenComparing(Demo::getAge));
        demoArray.forEach(demo -> {
            System.out.print("年龄 "+demo.getAge() +"  性别 " +demo.getSex()+"     ");
        });
        System.out.println();
        //多条件倒序
        System.out.println("多条件排序倒序 sex  倒序****************");
        demoArray.sort(Comparator.comparing(Demo::getSex).reversed().thenComparing(Demo::getAge));
        demoArray.forEach(demo -> {
            System.out.print("年龄 "+demo.getAge() +"  性别 " +demo.getSex()+"     ");
        });
        System.out.println();



        //按照年龄分组
         System.out.println("根据age分组结果为Map****************");
        Map<String, List<Demo>> demoOder = demos.stream().collect(Collectors.groupingBy(Demo::getAge));
        System.out.println("根据age分组结果:"+demoOder);

        //学号-名称
        Map<Integer,String> noMap =list.stream().collect(Collectors.toMap(Student::getNo, Student::getName,(o,n)-> n));
        System.out.println("学号-名称:"+noMap);

        // list to map 分组 marge  根据性别分组 获取每个组的age集合
        Map<Integer, List<String>> map = list.stream().collect(Collectors.toMap(Student::getSex, a -> new ArrayList() {{add(a.getAge());}},
                        (List<String> nVL, List<String> oVL) -> {
                            oVL.addAll(nVL);
                            return oVL;
                        }));

        System.out.println("根据性别分组 获取每个组的age集合:"+map);
    }
}

class Demo{
    private String name;
    private  Integer sex;
    private  String age;

    public Demo(String age,Integer sex) {
        this.age =age;
        this.sex =sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}