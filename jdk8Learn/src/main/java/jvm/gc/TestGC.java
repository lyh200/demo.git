package jvm.gc;

public class TestGC {
    private static final int _1MB = 1024 * 1024;

    public static void main(String[] args) {
        testAllocation();
    }

    /***
     * 参数说明：
     * Xms: heap init size
     * Xmx: heap max size
     * Xmn: young gen size
     * Xss: every thread stack size
     *
     *
     * vm param:  -verbose:gc -Xms20M -Xmx20M -Xmn10M  -XX:+PrintGCDetails -XX:SurvivorRatio=8
     *
     * print :
     * [GC (Allocation Failure) Disconnected from the target VM, address: '127.0.0.1:15090', transport: 'socket'
     * [PSYoungGen: 6489K->751K(9216K)] 6489K->4847K(19456K), 0.0207778 secs] [Times: user=0.01 sys=0.00, real=0.03 secs]
     * Heap
     *  PSYoungGen      total 9216K, used 7105K [0x00000000ff600000, 0x0000000100000000, 0x0000000100000000)
     *   eden space 8192K, 77% used [0x00000000ff600000,0x00000000ffc349e8,0x00000000ffe00000)
     *   from space 1024K, 73% used [0x00000000ffe00000,0x00000000ffebbcc0,0x00000000fff00000)
     *   to   space 1024K, 0% used [0x00000000fff00000,0x00000000fff00000,0x0000000100000000)
     *  ParOldGen       total 10240K, used 4096K [0x00000000fec00000, 0x00000000ff600000, 0x00000000ff600000)
     *   object space 10240K, 40% used [0x00000000fec00000,0x00000000ff000020,0x00000000ff600000)
     *  Metaspace       used 3100K, capacity 4568K, committed 4864K, reserved 1056768K
     *   class space    used 332K, capacity 392K, committed 512K, reserved 1048576K
     */
    private static void testAllocation() {
        byte[] allocation1, allocation2, allocation3, allocation4;
        allocation1 = new byte[2 * _1MB];
        allocation2 = new byte[2 * _1MB];
        allocation3 = new byte[2 * _1MB];
        allocation4 = new byte[4 * _1MB];
    }
}
