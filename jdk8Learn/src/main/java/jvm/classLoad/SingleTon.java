package jvm.classLoad;

/***
 * 类加载机制
 *   加载 -- 验证  --准备-- 解析 --初始化 -- 使用 --卸载
 *
 * 执行过程
 *   首先  java 类加载过程 static 修饰的 变量 是按照顺序来的
 *
 */
  public class SingleTon {

    private static SingleTon singleTon =  new SingleTon();
    public static int  count1;
    public static int  count2 =0;


    private SingleTon(){
        count1++;
        count2++;
        System.out.println(" instant init ..." + count1);
        System.out.println(" instant init ..." + count2);

    }
    public static SingleTon getSingleTon(){

        return singleTon;
    }
}
 class Test{
     public static void main(String[] args) {
         SingleTon singleTon =SingleTon.getSingleTon();
         System.out.println("count1="+ singleTon.count1);
         System.out.println("count1="+ singleTon.count2);
     }
    }
