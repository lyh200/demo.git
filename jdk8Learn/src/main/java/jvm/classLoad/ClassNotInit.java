package jvm.classLoad;

public class ClassNotInit {
    public static void main(String[] args) {
        System.out.println(Son.value);
      //  System.out.println(Constant.value);
    }
}
class Constant{

    static {
        System.out.println("constant");
    }
    public final static  int value =1;


}

class Parent{
    static {
        System.out.println("parent");
    }
    public static  int value =1;
}
class Son extends  Parent{
    static {
        System.out.println("son");
    }

}
