# jstat
___
jstat 命令可以查看堆内存各部分的使用量，以及加载类的数量。

Jstat位于java的bin目录下，主要利用JVM内建的指令对Java应用程序的资源和性能进行实时的命令行的监控，包括了对Heap size和垃圾回收状况的监控
　　命令的格式如下：

　　　　jstat [-命令选项] [vmid] [间隔时间/毫秒] [查询次数]
___
## 指令(基于jdk8)
    https://docs.oracle.com/javase/8/docs/technotes/tools/unix/jstat.html
	命令格式: jstat [-命令选项] [vmid] [间隔时间/毫秒] [查询次数]
   ### 查询用法
       jstat -option   




   ### 类加载统计
    jstat -class  1235



            参数说明
            Loaded:加载class的数量
            Bytes：所占用空间大小
            Unloaded：未加载数量
            Bytes:未加载占用空间
            Time：时间

   ### 编译统计
    jstat -compiler 19570



            参数说明
            Compiled：编译数量
            Failed：失败数量
            Invalid：不可用数量
            Time：时间
            FailedType：失败类型
            FailedMethod：失败的方法


   ### 垃圾回收统计
    jstat -gc  1235




            参数说明:
            S0C：s0的大小
            S1C：s1的大小
            S0U：s0的使用大小
            S1U：s1使用大小
            EC：eden的大小
            EU：eden园区的使用大小
            OC：老年代大小
            OU：老年代使用大小
            MC：方法区大小
            MU：方法区使用大小
            CCSC:压缩类空间大小
            CCSU:压缩类空间使用大小
            YGC：年轻代垃圾回收次数
            YGCT：年轻代垃圾回收消耗时间
            FGC：Full GC次数
            FGCT：Full GC消耗时间
            GCT：垃圾回收消耗总时间
