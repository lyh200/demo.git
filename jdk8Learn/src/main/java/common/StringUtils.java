package common;

public class StringUtils {

    /***
     * 判断字符串是否是回文
     */
    public static boolean isABBA(String str){
        if(str== null || str.length() ==0){
            return false;
        }
        char[] array = str.toCharArray();
        int left =0;
        int right =str.length()-1;
        while(left < right){
            if(array[left] != array[right]){
                return false;
            }
            left++;
            right--;
        }
        return true;
    }

//    public static void main(String[] args) {
//        System.out.println(isABBA("abab"));
//        System.out.println(isABBA("aabb"));
//        System.out.println(isABBA("aabbccbbaa"));
//        String str ="123456@2018";
//        int m = str.indexOf("4");
//        int n = str.indexOf("2",m);
//
//        System.out.println(m);
//        System.out.println(n);
//        System.out.println(str.substring(n));
//        System.out.println(str.substring(n,9));
//    }
}
