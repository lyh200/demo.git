import java.util.EventObject;

/**
 * Created by eason on 2017/10/4.
 */
public class Listener extends EventObject {
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public Listener(Object source) {
        super(source);
    }
}
