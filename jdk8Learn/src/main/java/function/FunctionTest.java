package function;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.function.Function;

public class FunctionTest {


    private static void Function1(Student student1, SFunction<Student, Student> function) {
        Student student = function.andThen(t -> {
            t.setLevel((t.getScore()/10L-11)*-1);
            t.setGrade(t.getLevel()+"级");
            return t;
        }).apply(student1);
        System.out.println(student);


    }

    public static void main(String[] args) {
        Student student1 = new Student(100L, "张三", null, null);
        Student student2 = new Student(80L, "李四", null, null);
        Student student3 = new Student(90L, "晚五", null, null);
        Student student4 = new Student(70L, "李二狗", null, null);
        Student student5 = new Student(40L, "婷婷", null, null);
        Student student6 = new Student(20L, "婷婷2", null, null);
        Student student7 = new Student(5L, "婷婷3", null, null);
        Function1(student1, t -> student1);
        Function1(student2, t -> student2);
        Function1(student3, t -> student3);
        Function1(student4, t -> student4);
        Function1(student5, t -> student5);
        Function1(student6, t -> student6);
        Function1(student7, t -> student7);
    }


    @Data
    @AllArgsConstructor
    @ToString
    static
    class Student {
        private Long score;
        private String name;
        private Long Level;
        private String grade;

    }
}
