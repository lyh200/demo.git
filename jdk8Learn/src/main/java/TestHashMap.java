import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by eason on 2018/4/13.
 */
public class TestHashMap {
    public static void main(String[] args) {
        List<String> list1 = new ArrayList<>();
        list1.add("a");
        list1.add("b");
        list1.add("c");
        list1.add("d");
        TestHashMap testHashMap = new TestHashMap();
        List<String> list = new ArrayList<>();
        list1.forEach(list::add);
        System.out.println(list);
        list1.forEach(testHashMap::print);
    }

    public  void print(String str) {
        System.out.println(str);
    }
    public  void print(String str,String str1) {
        System.out.println(str);
    }
}
