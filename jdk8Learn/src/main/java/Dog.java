
class Animal {
    public final void run() {
        System.out.println("animal");
    }
}

public class Dog {

    private  final void run() {
        System.out.println("dog");

    }
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.run();
    }

}

class DogTest {

}
