public class Test5 {
    public static void main(String[] args) {

        Node node13 =new Node(6,null);
        Node node12 =new Node(5,node13);
        Node node11 =new Node(5,node12);
        Node node10 =new Node(4,node11);
        Node node9 =new Node(4,node10);
        Node node8 =new Node(4,node9);
        Node node7 =new Node(4,node8);
        Node node6 =new Node(3,node7);
        Node node5 =new Node(2,node6);
        Node node4 =new Node(2,node5);
        Node node3 =new Node(1,node4);
        Node node2 =new Node(1,node3);
        Node node1 =new Node(1,node2);
        print(node1);
        System.out.println();

        System.out.println("_________");
        print(DuplicateRemoval(node1));

    }





    public static Node DuplicateRemoval(Node node) {
        if (node != null) {
            Integer value = node.value;
            Node temp =node.next;
            while (temp != null) {
                if (!value.equals(temp.value)) {
                    node.next=temp;
                } else {
                    temp = temp.next;
                    node=node.next;
                }
                System.out.println();
                print(node);
            }
        }

        return node;
    }

    public static void  print(Node node) {
            while (node != null) {
                System.out.print(node.value+",");
                node = node.next;
        }
    }
}

class Node {
    int value;

    public Node(int value, Node next) {
        this.value = value;
        this.next = next;
    }

    Node next;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    @Override
    public String toString(){
        String str ="";
        Node node =this;
        while (node != null) {
            str =str + node.value+",";
            node = node.next;
        }
        return str;
    }
}
