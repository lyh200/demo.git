package JDKProxyAndCGlibProxy;

/**
 * Created by eason on 2017/10/10.
 */
public interface UserManager {
    public void addUser(String id, String password);
    public void delUser(String id);
}
