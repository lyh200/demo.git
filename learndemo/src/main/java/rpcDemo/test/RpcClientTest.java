package rpcDemo.test;

import rpcDemo.core.RpcClient;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class RpcClientTest {

    private static final int threadNum = 2500;
    /***倒计时数 发令枪 用于制造线程的并发执行***/
    private static CountDownLatch cdl = new CountDownLatch(threadNum);

    public static void main(String[] args)  {
        for (int i = 0; i < threadNum; i++) {
            System.out.println(i);
            new Thread(new Runnable() {

                @Override
                public void run() {
                    deal(Thread.currentThread().getName());
                    cdl.countDown();
                }
            }).start();
        }

        try {
            cdl.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(threadNum+"个线程已经执行完毕！开始计算排名");
    }

    private static void deal(String str) {
        IHelloService service = RpcClient.getRemoteProxyObj(IHelloService.class, new InetSocketAddress("localhost", 8087));
//        System.out.println(service.sayHi(str, "新年快乐，大吉大利!"));
        Map<String, Object> param = new HashMap<>();
        param.put("1",str);
        param.put("2", str);
        param.put("3", "chaina");
        param.put("4", "chaina");
        param.put("4", "chaina");
        System.out.println(service.sayMap(param).toString());
    }

    public static void testCountDownLatch() {

        int threadCount = 10;

        final CountDownLatch latch = new CountDownLatch(threadCount);


    }
}
