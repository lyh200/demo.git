package rpcDemo.test;


import rpcDemo.core.IRpcService;

import java.util.Map;

/**
 * 服务端暴露服务接口，客户端可以直接调用
 */
public interface IHelloService extends IRpcService {
    String sayHi(String name, String msg);

    Map<String,Object> sayMap(Map<String,Object> param);
}

