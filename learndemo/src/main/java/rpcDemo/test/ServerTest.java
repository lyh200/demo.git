package rpcDemo.test;

import rpcDemo.core.Server;
import rpcDemo.core.ServerCenter;

import java.io.IOException;

public class ServerTest {
    public static void main(String[] args) throws IOException {
        Server server = new ServerCenter();
        server.register(IHelloService.class,HelloServiceImpl.class);
        server.start();

        server.stop();
    }
}
